package com.herick.foo.security.x509.excecoes;

/**
 * Classe para lancar exceções relacionadas ao processo de autorização.
 */
public class AutorizationException extends RuntimeException {

	private static final long serialVersionUID = -8788791261292036144L;

	public AutorizationException() {
        super();
    }

    public AutorizationException(Throwable throwable) {
        super(throwable);
    }

    public AutorizationException(final String mensagem) {
        super(mensagem);
    }

    public AutorizationException(final String mensagem, Throwable throwable) {
        super(mensagem, throwable);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
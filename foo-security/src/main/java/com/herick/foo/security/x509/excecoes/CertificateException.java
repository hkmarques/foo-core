package com.herick.foo.security.x509.excecoes;

/**
 * Classe para lancar excecoes relacionadas a certificados digitais.
 */
public class CertificateException extends RuntimeException {

	private static final long serialVersionUID = -2721867455251151813L;

	public CertificateException() {
        super();
    }

    public CertificateException(Throwable throwable) {
        super(throwable);
    }

    public CertificateException(final String mensagem) {
        super(mensagem);
    }

    public CertificateException(final String mensagem, Throwable throwable) {
        super(mensagem, throwable);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
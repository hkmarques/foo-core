package com.herick.foo.security.x509;

/**
 * Enum usado na aplicação para indicar o tipo de certificado
 */
public enum CertificateType {
	TYPE_1("1"),
	TYPE_2("2");

	private String description;

	CertificateType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
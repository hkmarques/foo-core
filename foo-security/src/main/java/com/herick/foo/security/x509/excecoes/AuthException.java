package com.herick.foo.security.x509.excecoes;

/**
 * Classe para lancar exceções relacionadas ao processo de autenticação.
 */
public class AuthException extends RuntimeException {

	private static final long serialVersionUID = -6152677931686968385L;

	public AuthException() {
        super();
    }

    public AuthException(Throwable throwable) {
        super(throwable);
    }

    public AuthException(final String mensagem) {
        super(mensagem);
    }

    public AuthException(final String mensagem, Throwable throwable) {
        super(mensagem, throwable);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
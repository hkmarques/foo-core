package com.herick.foo.core.utils.logging.aspects;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hibernate.SQLQuery;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Aspect
@Component
@EnableAspectJAutoProxy
public class LoggingAspect {
	
	Logger logger = Logger.getLogger(LoggingAspect.class);
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Before("@annotation(com.herick.foo.core.utils.logging.annotations.Logging)")
	public void gerarHistorico(JoinPoint joinPoint) {
		logger.info("::::::::::::::::::: Invocando histórico antes da operação " + joinPoint.toShortString());
		String sql= "{call <THERE WAS SOME PROCEDURE HERE>()}";
        final Query query = entityManager.createNativeQuery(sql);
        // TODO: AVALIAR TABELAS INDICADAS NO addSynchronizedQuerySpace (tabelas cuja cache será invalidada pela query)
        query.unwrap(SQLQuery.class).addSynchronizedQuerySpace("");
        query.executeUpdate();
	}

	@Before("@annotation(com.herick.foo.core.utils.logging.annotations.CorporateLogging)")
	public void gerarHistoricoCorporativo(JoinPoint joinPoint) {
		logger.info("::::::::::::::::::: Invocando histórico antes da operação " + joinPoint.toShortString());
		String sql= "{call <THERE WAS SOME OTHER PROCEDURE HERE>()}";
        final Query query = entityManager.createNativeQuery(sql);
        // TODO: AVALIAR TABELAS INDICADAS NO addSynchronizedQuerySpace (tabelas cuja cache será invalidada pela query)
        query.unwrap(SQLQuery.class).addSynchronizedQuerySpace("");
        query.executeUpdate();
	}
}
package com.herick.foo.core.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import java.io.Serializable;

/**
 * Superclasse para classes do domínio.
 */
public abstract class Entity<ID extends Serializable> implements Serializable {

	private static final long serialVersionUID = 4836139115617507659L;

	public abstract ID getId();

    public abstract void setId(ID id);

    /**
     * Implementação genérica para método equals considerando valores de propriedades básicas.
     *
     * @param   objeto o objeto a ser comparado com o chamador
     * @return  <code>true</code> caso as propriedades básicas sejam equivalentes, <code>false</code> caso contrário
     */
    @Override
    public boolean equals(Object objeto) {
        return EqualsBuilder.reflectionEquals(this, objeto);
    }

    /**
     * Implementação genérica para método toString considerando valores de propriedades básicas.
     *
     * @return  uma string representativa do objeto chamador
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
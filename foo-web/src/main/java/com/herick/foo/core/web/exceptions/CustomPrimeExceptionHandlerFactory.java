package com.herick.foo.core.web.exceptions;

import org.primefaces.application.exceptionhandler.PrimeExceptionHandlerFactory;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * Fábrica do handler customizado de exceções.
 */
public class CustomPrimeExceptionHandlerFactory extends PrimeExceptionHandlerFactory {

    public CustomPrimeExceptionHandlerFactory(ExceptionHandlerFactory wrapped) {
        super(wrapped);
    }

    /**
     * Instancia e retorna o handler customizado.
     * @return uma instância do handler customizado
     */
    @Override
    public ExceptionHandler getExceptionHandler() {
        return new CustomExceptionHandler(this.getWrapped().getExceptionHandler());
    }

}
package com.herick.foo.core.web.utils;

import org.primefaces.application.exceptionhandler.ExceptionInfo;
import java.io.Serializable;

/**
 * Tipo customizado para expor mais informações ao lançar exceção para camada de apresentação.
 */
public class CustomExceptionInfo extends ExceptionInfo implements Serializable {

	private static final long serialVersionUID = -4108705034401678660L;
	
	/**
     * Usado para armazenar JSON equivalente à exceção.
     */
    private String excecaoValidacaoJson;

    /**
     * Expõe no handler, em JSON, o conteúdo da exceção lançada.
     *
     * @return valor JSON correspondente à exceção lançada
     */
    public String getExcecaoValidacaoJson() {
        return excecaoValidacaoJson;
    }

    public void setExcecaoValidacaoJson(String excecaoValidacaoJson) {
        this.excecaoValidacaoJson = excecaoValidacaoJson;
    }
}
package com.herick.foo.core.web;

import com.herick.foo.core.business.facades.BusinessGenericFacade;
import com.herick.foo.core.business.dto.EntityDto;
import com.herick.foo.core.web.messages.FooMessagesJsf;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import java.io.Serializable;
import java.util.List;

@SuppressWarnings("rawtypes")
@ManagedBean
public abstract class GenericController<TEntity, TEntityDto, ID extends Serializable> implements Serializable {
	
	private static final long serialVersionUID = 8273207508699336713L;
	
	// A instância do controlador de mensagens usada pelo controller genérico.
	@ManagedProperty(name="redeReceitaMessagesJsf", value="#{redeReceitaMessagesJsf}")
	private FooMessagesJsf fooMessagesJsf;
	
	// A instância de negócio usada pelo controller genérico.
	private BusinessGenericFacade<TEntity, TEntityDto, ID> negocio;

	// A instância da entidade (VO) associada ao controller genérico.
	private TEntityDto EntityDto;
	
	// Lista de entidades (VO) associadas ao controller genérico.
	private List<TEntityDto> listaEntidadesVo;
    
    // A identificação do responsável pelo histórico das operações de escrita (matrícula ou id da pessoa)
    protected String responsavelHistorico;
    
    // Controle de visibilidade de elementos de tela
    protected boolean btnIncluirVisivel = true;
    protected boolean btnAlterarVisivel = true;
    protected boolean btnExcluirVisivel = true;
    protected boolean btnLimparVisivel = true;
    protected boolean btnCancelarVisivel = true;
    
    // region # Métodos abstratos

    /**
     * Método executado logo após a construção da instância pelo contexto do Spring.<br>
     * Aqui devem ser inicializados os campos usados nas operações do controller genérico.<br>
     * Na criação de uma subclasse do controller genérico, neste método deverá ser setado
     * o negócio específico da entidade associada ao controller.<br><br>
     * Ex. (Implementação do controller da '<code>EntidadeAbc</code>'):
     * <pre><code>
     *
     * {@literal @}Autowired
     * private EntidadeAbcNegocio entidadeAbcNegocio;
     *
     *
     * protected void init() {
     *      this.setBusiness(entidadeAbcNegocio);
     * }
     * </code></pre>
     * @throws Exception exceções lançadas na inicialização das classes de controle
     */
    @PostConstruct
    protected abstract void init() throws Exception;

    /**
     * Método responsável por instanciar o VO associado ao controller.
     * @throws Exception exceções lançadas na inicialização das classes de controle
     */
    protected abstract void initVo() throws Exception;

    // endregion

    // region # Métodos públicos

    // region # Actions básicas

    /**
     * Responsável por tratar a ação de inclusão da entidade associada ao controller.
     * @throws Exception exceções lançadas nas operações de inclusão
     */
    public void incluir() throws Exception {
        this.EntityDto = this.negocio.insert(this.EntityDto);
        fooMessagesJsf.showSuccessMsg("apresentacao_sucesso_inclusao");
        this.limpar();
    }

    /**
     * Responsável por tratar a ação de atualização da entidade associada ao controller.
     * @throws Exception exceções lançadas nas operações de alteração
     */
    public void alterar() throws Exception {
        this.EntityDto = this.negocio.edit(this.EntityDto);
        fooMessagesJsf.showSuccessMsg("apresentacao_sucesso_alteracao");
        this.limpar();
    }

    /**
     * Responsável por tratar a ação de exclusão da entidade associada ao controller.
     * @throws Exception exceções lançadas nas operações de exclusão
     */
    public void excluir() throws Exception {
        this.negocio.delete(this.EntityDto);
        fooMessagesJsf.showSuccessMsg("apresentacao_sucesso_exclusao");
    }
    
    /**
     * Responsável por tratar a ação de exclusão da entidade associada ao controller, informando-se explicitamente o dto.
     * @param EntityDto o objeto a ser excluído
     * @throws Exception exceções lançadas nas operações de exclusão
     */
    public void excluir(TEntityDto EntityDto) throws Exception {
        this.negocio.delete(EntityDto);
        fooMessagesJsf.showSuccessMsg("apresentacao_sucesso_exclusao");
    }

    /**
     * Responsável por tratar a ação de exclusão da entidade associada ao controller, informando-se explicitamente o id.
     * @param id o identificador do objeto a ser excluído
     * @throws Exception exceções lançadas nas operações de exclusão
     */
    public void excluir(ID id) throws Exception {
        this.negocio.delete(this.negocio.find(id));
        fooMessagesJsf.showSuccessMsg("apresentacao_sucesso_exclusao");
    }

    /**
     * Responsável por tratar a ação de consulta (individual) da entidade associada ao controller.
     * @throws Exception exceções lançadas nas operações de consulta
     */
    @SuppressWarnings("unchecked")
    public void consultar() throws Exception {
        this.EntityDto = this.negocio.find((ID) ((EntityDto) this.EntityDto).getId());
    }

    /**
     * Método responsável por limpar todos os atributos da tela.
     * @throws Exception exceções lançadas nas operações de limpeza dos dados da tela
     */
    public void limpar() throws Exception {
        this.initVo();
    }

    // endregion

	// region # Getters e Setters
    
	public TEntityDto getEntityDto() {
		return EntityDto;
	}

	public void setEntityDto(TEntityDto EntityDto) {
		this.EntityDto = EntityDto;
	}

	public List<TEntityDto> getListaEntidadesVo() {
		return listaEntidadesVo;
	}

	public void setListaEntidadesVo(List<TEntityDto> listaEntidadesVo) {
		this.listaEntidadesVo = listaEntidadesVo;
	}

	public void setNegocio(BusinessGenericFacade<TEntity, TEntityDto, ID> negocio) {
		this.negocio = negocio;
	}

	public FooMessagesJsf getFooMessagesJsf() {
		return fooMessagesJsf;
	}

	public void setFooMessagesJsf(FooMessagesJsf fooMessagesJsf) {
		this.fooMessagesJsf = fooMessagesJsf;
	}

	/**
	 * Retorna a identificação do responsável pelo histórico nas operações de escrita
	 * @return a identificação do responsável pelo histórico nas operações de escrita (matrícula ou id da pessoa)
	 */
	public String getResponsavelHistorico() {
		return responsavelHistorico;
	}

	public void setResponsavelHistorico(String responsavelHistorico) {
		this.responsavelHistorico = responsavelHistorico;
	}

	public boolean isBtnIncluirVisivel() {
		return btnIncluirVisivel;
	}

	public void setBtnIncluirVisivel(boolean btnIncluirVisivel) {
		this.btnIncluirVisivel = btnIncluirVisivel;
	}

	public boolean isBtnAlterarVisivel() {
		return btnAlterarVisivel;
	}

	public void setBtnAlterarVisivel(boolean btnAlterarVisivel) {
		this.btnAlterarVisivel = btnAlterarVisivel;
	}

	public boolean isBtnExcluirVisivel() {
		return btnExcluirVisivel;
	}

	public void setBtnExcluirVisivel(boolean btnExcluirVisivel) {
		this.btnExcluirVisivel = btnExcluirVisivel;
	}

	public boolean isBtnLimparVisivel() {
		return btnLimparVisivel;
	}

	public void setBtnLimparVisivel(boolean btnLimparVisivel) {
		this.btnLimparVisivel = btnLimparVisivel;
	}

	public boolean isBtnCancelarVisivel() {
		return btnCancelarVisivel;
	}

	public void setBtnCancelarVisivel(boolean btnCancelarVisivel) {
		this.btnCancelarVisivel = btnCancelarVisivel;
	}

    // endregion
}
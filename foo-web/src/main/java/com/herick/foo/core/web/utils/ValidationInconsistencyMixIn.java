package com.herick.foo.core.web.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.herick.foo.core.messages.FooMessages;
import com.herick.foo.core.validations.ValidationInconsistency;

/**
 * Mixin para controlar propriedades serializáveis em <code>ValidationInconsistency</code>,
 * para o Jackson.
 * Propriedades essenciais: propriedade e mensagem (traduzida).
 *
 * @see ValidationInconsistency
 */
public abstract class ValidationInconsistencyMixIn {

    @JsonIgnore
    abstract FooMessages getProvedorMensagens();

    @JsonIgnore
    abstract String getMensagemChave();

    @JsonIgnore
    abstract String getMensagemParametros();

}

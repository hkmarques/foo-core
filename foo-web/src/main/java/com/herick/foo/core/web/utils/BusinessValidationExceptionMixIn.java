package com.herick.foo.core.web.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.herick.foo.core.validations.excecoes.BusinessValidationException;

/**
 * Mixin para controlar propriedades serializáveis em <code>BusinessValidationException</code>,
 * para o Jackson.
 *
 * @see BusinessValidationException
 */
public abstract class BusinessValidationExceptionMixIn {

    @JsonIgnore
    abstract String getStackTrace();

}

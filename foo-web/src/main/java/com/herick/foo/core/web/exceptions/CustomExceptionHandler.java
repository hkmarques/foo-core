package com.herick.foo.core.web.exceptions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.herick.foo.core.messages.FooMessages;
import com.herick.foo.core.utils.FooExceptionUtil;
import com.herick.foo.core.validations.ValidationInconsistency;
import com.herick.foo.core.validations.excecoes.BusinessValidationException;
import com.herick.foo.core.web.messages.FooMessagesJsf;
import com.herick.foo.core.web.utils.CustomExceptionInfo;
import com.herick.foo.core.web.utils.ValidationInconsistencyMixIn;
import com.herick.foo.core.web.utils.BusinessValidationExceptionMixIn;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.primefaces.application.exceptionhandler.ExceptionInfo;
import org.primefaces.application.exceptionhandler.PrimeExceptionHandler;
import org.primefaces.context.RequestContext;
import javax.faces.FacesException;
import javax.faces.application.ProjectStage;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

/**
 * Classe responsável pelo tratamento customizado de exceções na camada de apresentação.
 */
public class CustomExceptionHandler extends PrimeExceptionHandler {

	private ObjectMapper jacksonObjectMapper = new ObjectMapper();
	
	private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
    private static final Logger logger = Logger.getLogger(CustomExceptionHandler.class);

    public CustomExceptionHandler(ExceptionHandler wrapped) {
        super(wrapped);
        setupSerialization();
    }

    @Override
    public void handle() throws FacesException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	// TODO: Verificar uma forma de injetar estas dependências para evitar problemas em eventuais mudanças na criação das classes de mensagens
    	FooMessagesJsf fooMessagesJsf = new FooMessagesJsf();

        if (context.getResponseComplete()) {
            return;
        }

        Iterable<ExceptionQueuedEvent> exceptionQueuedEvents = getUnhandledExceptionQueuedEvents();
        if (exceptionQueuedEvents != null && exceptionQueuedEvents.iterator() != null) {
            Iterator<ExceptionQueuedEvent> unhandledExceptionQueuedEvents = getUnhandledExceptionQueuedEvents().iterator();

            if (unhandledExceptionQueuedEvents.hasNext()) {
                try {
                    Throwable throwable = unhandledExceptionQueuedEvents.next().getContext().getException();
                    unhandledExceptionQueuedEvents.remove();

                    Throwable rootCause = getRootCause(throwable);
                    ExceptionInfo info = createExceptionInfo(rootCause);

                    // print exception in development stage
                    if (context.getApplication().getProjectStage() == ProjectStage.Development) {
                        rootCause.printStackTrace();
                    }

                    // always log the exception
                    logger.log(Level.FATAL, FooExceptionUtil.extractExceptionMessage(throwable), rootCause);

                    // Adiciona as informações detalhadas da exceção aos parâmetros na sessão do usuário
                    // Esta implementação visa corrigir um bug nativo do Mojarra na passagem de parâmetros via Flash no caso de redirecionamentos
                    context.getExternalContext().getSessionMap().put("exceptionInfo", info);

                    if (context.getPartialViewContext().isAjaxRequest()) {
                    	// TODO: avaliar a possibilidade de injeção da depêndencia, caso haja mudança na forma de criação do '...'
                    	fooMessagesJsf.showErrorMsg(null, info.getMessage());
                        handleAjaxException(context, rootCause, info);
                    } else {
                    	redirectErrors(context, rootCause);
                    }
                } catch (Exception ex) {
                	final String mensagemErroHandler = this.getMessageProvider().getTranslatedMessage("apresentacao_exceptionhandler_erro") + " - " + FooExceptionUtil.extractExceptionMessage(ex);
                    logger.log(Level.FATAL, mensagemErroHandler, ex);
                    throw new FacesException(mensagemErroHandler);
                }
            }

            while (unhandledExceptionQueuedEvents.hasNext()) {
                // Any remaining unhandled exceptions are not interesting. First fix the first.
                unhandledExceptionQueuedEvents.next();
                unhandledExceptionQueuedEvents.remove();
            }
        }
    }

    public void redirectErrors(FacesContext context, Throwable rootCause) {
    	// Redireciona usuário à página de erro padrão para a exibição das informações da exceção
		context.getApplication().getNavigationHandler().handleNavigation(context, null, this.returnErrorPage(context, rootCause) + "?faces-redirect=true&includeViewParams=true");
    }
    
    /**
     * Método responsável por adicionar configurações específicas à serialização (Jackson) de dados das exceções.
     */
    public void setupSerialization() {
        this.jacksonObjectMapper.addMixIn(ValidationInconsistency.class, ValidationInconsistencyMixIn.class);
        this.jacksonObjectMapper.addMixIn(BusinessValidationException.class, BusinessValidationExceptionMixIn.class);
    }

    @Override
    protected ExceptionInfo createExceptionInfo(Throwable rootCause) throws IOException {
        CustomExceptionInfo info = new CustomExceptionInfo();
        info.setException(rootCause);
        info.setStackTrace(rootCause.getStackTrace());
        info.setTimestamp(new Date());
        info.setType(rootCause.getClass().getName());
        BusinessValidationException businessValidationException = getCauseBusinessValidationException(rootCause);
        if (businessValidationException != null) {
            // Monta retorno em JSON para exibição das marcações de validação nos elementos de formulários com o uso de Javascript
        	String jsonExcecaoValidacao = this.convertExceptionIntoJSON(businessValidationException);
            info.setExcecaoValidacaoJson(jsonExcecaoValidacao);
            // Como se trata de uma exceção de validação, a mensagem padrão da exceção será substituída pela mensagem resumida de inconsistências
            info.setMessage(businessValidationException.getMensagemResumidaInconsistencias());
        } else {
        	// Para os demais casos, a mensagem padrão montada pelo tratamento de exceções deverá ser enviada à tela
        	info.setMessage(rootCause.getMessage());
        }

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        rootCause.printStackTrace(pw);
        String stackTraceFormatada = FooExceptionUtil.formatStacktrace(sw.toString()).replaceAll("(\r\n|\n)", "<br/>");
        info.setFormattedStackTrace(stackTraceFormatada);
        pw.close();
        sw.close();

        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN);
        info.setFormattedTimestamp(format.format(info.getTimestamp()));

        return info;
    }

    /**
     * Método recursivo usado para extrair a partir da exceção informada a causa do tipo <code>BusinessValidationException</code>,
     * caso haja uma.
     *
     * @param throwable a exceção a partir da qual espera-se extrair uma causa do tipo <code>BusinessValidationException</code>
     * @return          a causa do tipo <code>BusinessValidationException</code> caso haja uma, e <code>null</code> caso contrário
     */
    private BusinessValidationException getCauseBusinessValidationException(Throwable throwable) {
        BusinessValidationException causa;
        if (throwable == null || BusinessValidationException.class.isInstance(throwable)) {
            causa = (BusinessValidationException) throwable;
        } else {
            causa = getCauseBusinessValidationException(throwable.getCause());
        }

        return causa;
    }

    /**
     * Método utilizado para converter uma exceção para JSON e armazenar para tratamento client-side.
     *
     * @param throwable a exceção a ser convertida
     * @return          o resultado da conversão
     */
    private String convertExceptionIntoJSON(Throwable throwable) throws IOException {
        return jacksonObjectMapper.writer().writeValueAsString(throwable);
    }
    
    /**
     * Método responsável por retornar a página de erro definida para cada tipo de exceção.
     * 
     * 
     * @param context o contexto do JSF
     * @param rootCause a exceção original relacionada ao erro ocorrido
     * @return o caminho da página de erro (específica para a exceção ou a página de erro padrão)
     */
    private String returnErrorPage(FacesContext context, Throwable rootCause) {
		Map<String, String> errorPages = RequestContext.getCurrentInstance().getApplicationContext().getConfig().getErrorPages();
		// Busca página de erro pela exceção específica
		String errorPage = errorPages.get(rootCause.getClass().getName());
		// Caso não encontre a página de erro específica da exceção, busca pela página padrão
		if (errorPage == null) {
			errorPage = errorPages.get(null);
		}
		if (errorPage == null) {
			throw new IllegalArgumentException(this.getMessageProvider().getTranslatedMessage("tratamentoExcecoes_pagina_erro_indefinida", rootCause.getClass()));
		}
		return errorPage;
    }
    
    /**
     * Método responsável por retornar o provedor de mensagens padrão do sistema
     * 
     * @return o provedor de mensagens padrão do sistema
     */
    private FooMessages getMessageProvider() {
    	return new FooMessages();
    }
}
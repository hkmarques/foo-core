package com.herick.foo.core.web.messages;

import com.herick.foo.core.messages.FooMessages;
import com.herick.foo.core.utils.FooExceptionUtil;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import java.util.MissingResourceException;

@Component(value="redeReceitaMessagesJsf")
public class FooMessagesJsf {
	
	// Arquivo de mensagens gerais do sistema
	FooMessages fooMessages;
 	
 	// A instância de logger usada pelo controller genérico.
 	private final Logger logger = Logger.getLogger(FooMessagesJsf.class);
    
    public FooMessagesJsf() {
    	this.fooMessages = new FooMessages();
	}
    
    /**
     * Método para exibir mensagens de sucesso no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     */
	public void showSuccessMsg(String msgBodyKey) {
		this.showSuccessMsg(msgBodyKey, null);
	}
	
	/**
     * Método para exibir mensagens de sucesso no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     * @param msgBodyComplement complemento da mensagem a ser exibida (qualquer conteúdo textual a ser concatenado ao final da mensagem traduzida)
     */
	public void showSuccessMsg(String msgBodyKey, String msgBodyComplement) {
		this.showMsg("mensagensSucesso", FacesMessage.SEVERITY_INFO, "apresentacao_sucesso_titulo", msgBodyKey, msgBodyComplement);
	}

    /**
     * Método para exibir mensagens informativas no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     */
	public void showInfoMsg(String msgBodyKey) {
		this.showInfoMsg(msgBodyKey, null);
	}

	/**
     * Método para exibir mensagens informativas no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     * @param msgBodyComplement complemento da mensagem a ser exibida (qualquer conteúdo textual a ser concatenado ao final da mensagem traduzida)
     */
	public void showInfoMsg(String msgBodyKey, String msgBodyComplement) {
		this.showMsg(FacesMessage.SEVERITY_INFO, "apresentacao_info_titulo", msgBodyKey, msgBodyComplement);
	}
	
	/**
     * Método para exibir mensagens de alerta no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     */
	public void showWarningMsg(String msgBodyKey) {
		this.showWarningMsg(msgBodyKey, null);
	}
	
	/**
     * Método para exibir mensagens de alerta no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     * @param msgBodyComplement complemento da mensagem a ser exibida (qualquer conteúdo textual a ser concatenado ao final da mensagem traduzida)
     */
	public void showWarningMsg(String msgBodyKey, String msgBodyComplement) {
		this.showMsg(FacesMessage.SEVERITY_WARN, "apresentacao_warn_titulo", msgBodyKey, msgBodyComplement);
	}
	
	/**
     * Método para exibir mensagens de erro no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     */
	public void showErrorMsg(String msgBodyKey) {
		this.showErrorMsg(msgBodyKey, null);
	}
	
	/**
     * Método para exibir mensagens de erro no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     * @param msgBodyComplement complemento da mensagem a ser exibida (qualquer conteúdo textual a ser concatenado ao final da mensagem traduzida)
     */
	public void showErrorMsg(String msgBodyKey, String msgBodyComplement) {
		this.showMsg(FacesMessage.SEVERITY_ERROR, "apresentacao_error_titulo", msgBodyKey, msgBodyComplement);
	}
	
	/**
     * Método para exibir mensagens de erro no sistema.
     * @param throwable exceção de onde se deseja extrair a mensagem de erro
     */
	public void showErrorMsg(Throwable throwable) {
		this.showErrorMsg(null, FooExceptionUtil.extractExceptionMessage(throwable));
	}
	
	/**
     * Método para exibir mensagens de erro fatal no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     */
	public void showFatalErrorMsg(String msgBodyKey) {
		this.showFatalErrorMsg(msgBodyKey, null);
	}
	
	/**
     * Método para exibir mensagens de erro fatal no sistema.
     * @param msgBodyKey chave da mensagem a ser exibida (deve estar contida no arquivo de mensagens i18n)
     * @param msgBodyComplement complemento da mensagem a ser exibida (qualquer conteúdo textual a ser concatenado ao final da mensagem traduzida)
     */
	public void showFatalErrorMsg(String msgBodyKey, String msgBodyComplement) {
		this.showMsg(FacesMessage.SEVERITY_FATAL, "apresentacao_fatal_titulo", msgBodyKey, msgBodyComplement);
	}
    
	// region # Métodos privados

	private void showMsg(Severity severity, String msgTitleKey, String msgBodyKey, String msgBodyComplement) {
		showMsg(null, severity, msgTitleKey, msgBodyKey, msgBodyComplement);
	}

	private void showMsg(String idComponenteMessages, Severity severity, String msgTitleKey, String msgBodyKey, String msgBodyComplement) {
		String msgTitle = "";
		StringBuilder msgContentSb = new StringBuilder();
		try {
			// Traduz o título da mensagem a partir da chave passada como parâmetro
			if(msgTitleKey != null) {
				msgTitle = this.fooMessages.getTranslatedMessage(msgTitleKey);
			}
			// Traduz o corpo da mensagem a partir da chave passada como parâmetro
			if(msgBodyKey != null) {
				msgContentSb.append(this.fooMessages.getTranslatedMessage(msgBodyKey));
			}
			// Caso haja complemento, o mesmo deverá ser concatenado ao conteúdo da mensagem
			if(msgBodyComplement != null) {
				if(msgContentSb.length() > 0) {
					msgContentSb.append(" - ");
				}
				msgContentSb.append(msgBodyComplement);
			}
		} catch (MissingResourceException mre) {
			// Caso ocorram erros nas traduções das mensagens, os mesmos deverão ser exibidos na tela
			msgContentSb.append(FooExceptionUtil.extractExceptionMessage(mre));
			// Altera a severidade da mensagem para fatal
			severity = FacesMessage.SEVERITY_FATAL;
		}
		String msgContent = msgContentSb.toString();
		// Adiciona mensagem ao Faces Context (para ser exibida na tela para o usuário)
		FacesContext.getCurrentInstance().addMessage(idComponenteMessages, new FacesMessage(severity, msgTitle, msgContent));
		// Adiciona a mesma mensagem da tela ao log
		this.showLog(severity, msgContent);
	}

	private void showLog(Severity severity, String msgLog) {
		if(severity.equals(FacesMessage.SEVERITY_INFO)) {
			this.logger.info(msgLog);
		} else if(severity.equals(FacesMessage.SEVERITY_WARN)) {
			this.logger.warn(msgLog);
		} else if(severity.equals(FacesMessage.SEVERITY_ERROR) || severity.equals(FacesMessage.SEVERITY_FATAL)) {
			this.logger.error(msgLog);
		}
	}

	// endregion
}
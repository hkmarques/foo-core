package com.herick.foo.core.commons.testes;

import com.herick.foo.core.utils.DateTimeUtil;
import com.herick.foo.core.utils.DatePeriod;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Testes unitários associados à classe <code>DataPeriodo</code>.
 * @see DatePeriod
 */
@PrepareForTest(DateTimeUtil.class)
@RunWith(JUnitParamsRunner.class)
public class DataPeriodoTests {

    @Rule
    public PowerMockRule powerMockRule = new PowerMockRule();
	
    @Test
    @Parameters({
	    	"01/01/2000, 31/12/2014, Past",
            "01/01/2015, 31/12/2030, Present",
            "01/01/2030, 31/12/2050, Future"
    })
    public void testGetDataPeriodoStatus(String strDataInicial, String strDataFinal, String statusEsperado) throws Exception {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");

        Date dataAtualFixa = DateTime.parse("20/01/2015", dateTimeFormatter).toDate();

        // A seguir, o supra-sumo dos tests dependentes de data atual, sem alteração no código das classes testadas:
        spy(DateTimeUtil.class);
        when(DateTimeUtil.getCurrentDate()).thenReturn(dataAtualFixa);

    	// Converte parâmetros em datas
    	Date dataInicial = DateTime.parse(strDataInicial, dateTimeFormatter).toDate();
    	Date dataFinal = DateTime.parse(strDataFinal, dateTimeFormatter).toDate();
    	DatePeriod datePeriod = new DatePeriod(dataInicial, dataFinal);

    	assertEquals("O status obtido para o período é diferente do esperado", statusEsperado, datePeriod.getStatus().getDescription());
    }
    
    @Test
    @Parameters({
        "01/01/2015 00:00:00, Past",
        "20/01/2015 00:00:00, Past",
        "20/01/2015 12:00:00, Present",
        "01/01/2030 00:00:00, Present"
    })
    public void testGetDataPeriodoStatusDataInicialNula(String strDataFinal, String statusEsperado) throws Exception {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

        Date dataAtualFixa = DateTime.parse("20/01/2015 00:00:01", dateTimeFormatter).toDate();

        // A seguir, o supra-sumo dos tests dependentes de data atual, sem alteração no código das classes testadas:
        spy(DateTimeUtil.class);
        when(DateTimeUtil.getCurrentDateTime()).thenReturn(dataAtualFixa);
        
        // Força a data inicial a ser nula
        Date dataInicial = null;

    	// Converte parâmetros em datas
    	Date dataFinal = DateTime.parse(strDataFinal, dateTimeFormatter).toDate();

    	DatePeriod datePeriod = new DatePeriod(dataInicial, dataFinal);

    	assertEquals("O status obtido para o período é diferente do esperado", statusEsperado, datePeriod.getStatus().getDescription());
    }
    
    @Test
    @Parameters({
        "01/01/2015 00:00:00, Present",
        "20/01/2015 00:00:00, Present",
        "20/01/2015 00:00:01, Present",
        "20/01/2015 00:00:02, Future",
        "01/01/2030 00:00:00, Future"
    })
    public void testGetDataPeriodoStatusDataFinalNula(String strDataInicial, String statusEsperado) throws Exception {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

    	Date dataAtualFixa = DateTime.parse("20/01/2015 00:00:01", dateTimeFormatter).toDate();

        // A seguir, o supra-sumo dos tests dependentes de data atual, sem alteração no código das classes testadas:
        spy(DateTimeUtil.class);
        when(DateTimeUtil.getCurrentDateTime()).thenReturn(dataAtualFixa);

    	// Converte parâmetros em datas
    	Date dataInicial = DateTime.parse(strDataInicial, dateTimeFormatter).toDate();
    	
    	// Força a data final a ser nula
    	Date dataFinal = null;

    	DatePeriod datePeriod = new DatePeriod(dataInicial, dataFinal);

    	assertEquals("O status obtido para o período é diferente do esperado", statusEsperado, datePeriod.getStatus().getDescription());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetDataPeriodoNulo() throws Exception {
    	new DatePeriod(null, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetDataPeriodoInvalido() throws Exception {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
    	Date dataInicial = DateTime.parse("01/01/2015", dateTimeFormatter).toDate();
    	Date dataFinal = DateTime.parse("01/01/2014", dateTimeFormatter).toDate();
    	new DatePeriod(dataInicial, dataFinal);
    }
    
    @Test
    @Parameters({
    	"01/01/2015, true",
        "20/01/2015, true",
        "31/12/2015, true",
        "31/12/2014, false",
        "01/01/2016, false"
    })
    public void testDataPeriodoContains(String strData, boolean resultadoEsperado) {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
    	Date dataInicial = DateTime.parse("01/01/2015", dateTimeFormatter).toDate();
    	Date dataFinal = DateTime.parse("31/12/2015", dateTimeFormatter).toDate();
    	Date dataTeste = DateTime.parse(strData, dateTimeFormatter).toDate();
    	
    	DatePeriod datePeriod = new DatePeriod(dataInicial, dataFinal);
    	
    	assertEquals("O resultado obtido para o teste de data contida no período é diferente do esperado", resultadoEsperado, datePeriod.contains(dataTeste));
    }
    
    @Test
    @Parameters({
    	"01/01/2015, true",
        "20/01/2015, true",
        "31/12/2015, true",
        "31/12/2014, true",
        "01/01/2016, false"
    })
    public void testDataPeriodoContainsDataInicialNula(String strData, boolean resultadoEsperado) {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
    	Date dataFinal = DateTime.parse("31/12/2015", dateTimeFormatter).toDate();
    	Date dataTeste = DateTime.parse(strData, dateTimeFormatter).toDate();
    	
    	DatePeriod datePeriod = new DatePeriod(null, dataFinal);
    	
    	assertEquals("O resultado obtido para o teste de data contida no período é diferente do esperado", resultadoEsperado, datePeriod.contains(dataTeste));
    }
    
    @Test
    @Parameters({
    	"01/01/2015, true",
        "20/01/2015, true",
        "31/12/2015, true",
        "31/12/2014, false",
        "01/01/2016, true"
    })
    public void testDataPeriodoContainsDataFinalNula(String strData, boolean resultadoEsperado) {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
    	Date dataInicial = DateTime.parse("01/01/2015", dateTimeFormatter).toDate();
    	Date dataTeste = DateTime.parse(strData, dateTimeFormatter).toDate();
    	
    	DatePeriod datePeriod = new DatePeriod(dataInicial, null);
    	
    	assertEquals("O resultado obtido para o teste de data contida no período é diferente do esperado", resultadoEsperado, datePeriod.contains(dataTeste));
    }
    
    @Test
    @Parameters({
    		"01/01/2014, 31/12/2016,       01/01/2013, 31/12/2013,       false",
    		"01/01/2014, 31/12/2016,       01/01/2013, 01/01/2014,       true",
    		"01/01/2014, 31/12/2016,       01/01/2013, 31/12/2015,       true",
	    	"01/01/2014, 31/12/2016,       01/01/2015, 31/12/2015,       true",
	    	"01/01/2014, 31/12/2016,       01/01/2015, 31/12/2017,       true",
	    	"01/01/2014, 31/12/2016,       31/12/2016, 31/12/2017,       true",
	    	"01/01/2014, 31/12/2016,       01/01/2017, 31/12/2020,       false"
    })
    public void testDataPeriodoInterseccao(String strDataInicialPeriodoA, String strDataFinalPeriodoA, String strDataInicialPeriodoB, String strDataFinalPeriodoB, boolean resultadoEsperado) throws Exception {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");

    	// Converte parâmetros em datas
    	Date dataInicialPeriodoA = DateTime.parse(strDataInicialPeriodoA, dateTimeFormatter).toDate();
    	Date dataFinalPeriodoA = DateTime.parse(strDataFinalPeriodoA, dateTimeFormatter).toDate();
    	DatePeriod datePeriodA = new DatePeriod(dataInicialPeriodoA, dataFinalPeriodoA);
    	
    	Date dataInicialPeriodoB = DateTime.parse(strDataInicialPeriodoB, dateTimeFormatter).toDate();
    	Date dataFinalPeriodoB = DateTime.parse(strDataFinalPeriodoB, dateTimeFormatter).toDate();
    	DatePeriod datePeriodB = new DatePeriod(dataInicialPeriodoB, dataFinalPeriodoB);

    	assertEquals("O status obtido para o teste de intersecções de períodos é diferente do esperado", resultadoEsperado, datePeriodA.intersects(datePeriodB));
    }
    
    @Test
    @Parameters({
    		"31/12/2016,       01/01/2013, 31/12/2015,       true",
    		"31/12/2016,       01/01/2015, 31/12/2016,       true",
	    	"31/12/2016,       01/01/2015, 31/12/2017,       true",
	    	"31/12/2016,       31/12/2016, 31/12/2017,       true",
	    	"31/12/2016,       01/01/2017, 31/12/2020,       false"
    })
    public void testDataPeriodoInterseccaoDataInicialNulaPeriodoA(String strDataFinalPeriodoA, String strDataInicialPeriodoB, String strDataFinalPeriodoB, boolean resultadoEsperado) throws Exception {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");

    	// Converte parâmetros em datas
    	Date dataInicialPeriodoA = null;
    	Date dataFinalPeriodoA = DateTime.parse(strDataFinalPeriodoA, dateTimeFormatter).toDate();
    	DatePeriod datePeriodA = new DatePeriod(dataInicialPeriodoA, dataFinalPeriodoA);
    	
    	Date dataInicialPeriodoB = DateTime.parse(strDataInicialPeriodoB, dateTimeFormatter).toDate();
    	Date dataFinalPeriodoB = DateTime.parse(strDataFinalPeriodoB, dateTimeFormatter).toDate();
    	DatePeriod datePeriodB = new DatePeriod(dataInicialPeriodoB, dataFinalPeriodoB);

    	assertEquals("O status obtido para o teste de intersecções de períodos é diferente do esperado (ref. período A)", resultadoEsperado, datePeriodA.intersects(datePeriodB));
    	assertEquals("O status obtido para o teste de intersecções de períodos é diferente do esperado (ref. período B)", resultadoEsperado, datePeriodB.intersects(datePeriodA));
    }
    
    @Test
    @Parameters({
    		"01/01/2014,       01/01/2013, 31/12/2013,       false",
    		"01/01/2014,       01/01/2013, 01/01/2014,       true",
    		"01/01/2014,       01/01/2013, 31/12/2015,       true",
	    	"01/01/2014,       01/01/2014, 31/12/2015,       true",
	    	"01/01/2014,       31/12/2016, 31/12/2017,       true"
    })
    public void testDataPeriodoInterseccaoDataFinalNulaPeriodoA(String strDataInicialPeriodoA, String strDataInicialPeriodoB, String strDataFinalPeriodoB, boolean resultadoEsperado) throws Exception {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");

    	// Converte parâmetros em datas
    	Date dataInicialPeriodoA = DateTime.parse(strDataInicialPeriodoA, dateTimeFormatter).toDate();
    	DatePeriod datePeriodA = new DatePeriod(dataInicialPeriodoA, null);
    	
    	Date dataInicialPeriodoB = DateTime.parse(strDataInicialPeriodoB, dateTimeFormatter).toDate();
    	Date dataFinalPeriodoB = DateTime.parse(strDataFinalPeriodoB, dateTimeFormatter).toDate();
    	DatePeriod datePeriodB = new DatePeriod(dataInicialPeriodoB, dataFinalPeriodoB);

    	assertEquals("O status obtido para o teste de intersecções de períodos é diferente do esperado (ref. período A)", resultadoEsperado, datePeriodA.intersects(datePeriodB));
    	assertEquals("O status obtido para o teste de intersecções de períodos é diferente do esperado (ref. período B)", resultadoEsperado, datePeriodB.intersects(datePeriodA));
    }
    
    @Test
    public void testDataPeriodoInterseccaoPeriodoTesteNulo() throws Exception {
    	DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");

    	// Converte parâmetros em datas
    	Date dataInicialPeriodoA = DateTime.parse("01/01/2014", dateTimeFormatter).toDate();
    	Date dataFinalPeriodoA = DateTime.parse("31/12/2016", dateTimeFormatter).toDate();
    	DatePeriod datePeriodA = new DatePeriod(dataInicialPeriodoA, dataFinalPeriodoA);

    	assertFalse("O status obtido para o teste de intersecções de períodos é diferente do esperado", datePeriodA.intersects(null));
    }
}
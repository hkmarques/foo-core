package com.herick.foo.core.commons.testes;

import com.herick.foo.core.commons.testes.utils.ReflectionUtilTestClass;
import com.herick.foo.core.utils.ReflectionUtil;
import org.junit.Test;
import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.List;
import static org.junit.Assert.assertEquals;

/**
 * Testes associados ao utilitário para uso do recurso de reflection.
 */
public class ReflectionUtilTests {

    @Test
    @SuppressWarnings("rawtypes")
    public void testObterMetodosComAnotacao() throws Exception {
		Class classe = ReflectionUtilTestClass.class;
        List<Method> metodosAnotados = ReflectionUtil.obterMetodosComAnotacao(classe, Resource.class);

        assertEquals("A quantidade de métodos retornados é diferente da esperada", 2, metodosAnotados.size());
    }
}

package com.herick.foo.core.commons.testes.constants;

import com.herick.foo.core.constants.StatusYesNo;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class StatusYesNoTests {

    @Test
    @Parameters({
            "Y, YES",
            "N, NO"
    })
    public void testParseCharacter(Character valor, StatusYesNo resultadoEsperado) throws Exception {
        assertEquals(resultadoEsperado, StatusYesNo.parse(valor));
    }

    @Test
    @Parameters({
            "true, YES",
            "false, NO"
    })
    public void testParseBoolean(boolean valorBoolean, StatusYesNo resultadoEsperado) throws Exception {
        assertEquals(resultadoEsperado, StatusYesNo.parse(valorBoolean));
    }

}

package com.herick.foo.core.commons.testes;

import com.herick.foo.core.utils.FooExceptionUtil;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

/**
 * Testes associados ao utilitário para exceções.
 */
@RunWith(JUnitParamsRunner.class)
public class FooExceptionUtilTests {
	
	@Test
	@Parameters({
			"com.herick, <span class=\"panel-erros-conteudo-destaque\">com.herick</span>",
			"namespace.antes.com.herick.e.depois, <span class=\"panel-erros-conteudo-destaque\">namespace.antes.com.herick.e.depois</span>",
			"com.herick, <span class=\"panel-erros-conteudo-destaque\">com.herick</span>",
			"linha antes\ncom.herick\nlinha depois, linha antes\n<span class=\"panel-erros-conteudo-destaque\">com.herick</span>\nlinha depois",
			"com.herick com.herick, <span class=\"panel-erros-conteudo-destaque\">com.herick com.herick</span>"
	})
	public void testFormatarStacktraceExcecao(String stacktrace, String expected) throws Exception{
		String actual = FooExceptionUtil.formatStacktrace(stacktrace);
		assertEquals(expected, actual);
	}
}

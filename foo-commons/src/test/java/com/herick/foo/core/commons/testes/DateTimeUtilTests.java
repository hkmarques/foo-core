package com.herick.foo.core.commons.testes;

import com.herick.foo.core.utils.DateTimeUtil;
import junitparams.JUnitParamsRunner;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;
import java.util.Calendar;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Testes unitários associados à classe <code>DateTimeUtil</code>.
 * @see DateTimeUtil
 */
@PrepareForTest(DateTimeUtil.class)
@RunWith(JUnitParamsRunner.class)
public class DateTimeUtilTests {

    @Rule
    public PowerMockRule powerMockRule = new PowerMockRule();

    @Test
    public void testGetDataAtual() throws Exception {
        Date currentDate = DateTimeUtil.getCurrentDate();
        Calendar dataAtualCalendar = Calendar.getInstance();
        dataAtualCalendar.setTime(currentDate);

        Calendar dataAtualJVM = Calendar.getInstance();

        assertEquals(dataAtualJVM.get(Calendar.YEAR), dataAtualCalendar.get(Calendar.YEAR));
        assertEquals(dataAtualJVM.get(Calendar.MONTH), dataAtualCalendar.get(Calendar.MONTH));
        assertEquals(dataAtualJVM.get(Calendar.DATE), dataAtualCalendar.get(Calendar.DATE));

        assertEquals(0, dataAtualCalendar.get(Calendar.HOUR_OF_DAY));
        assertEquals(0, dataAtualCalendar.get(Calendar.MINUTE));
        assertEquals(0, dataAtualCalendar.get(Calendar.SECOND));
        assertEquals(0, dataAtualCalendar.get(Calendar.MILLISECOND));
    }

    @Test
    public void testGetCurrentDateTime() throws Exception {
        Long margemErroHoraMs = 500L;
        Date currentDateTime = DateTimeUtil.getCurrentDateTime();

        Date dataHoraAtualJVM = new Date();

        Long diferenca = Math.abs(currentDateTime.getTime() - dataHoraAtualJVM.getTime());
        assertTrue("A data retornada difere da obtida da JVM em " + diferenca + " ms. ", diferenca <= margemErroHoraMs);
    }

    @Test
    public void testGetDataPrimeiraHora() throws Exception {
        Calendar dataCalendar = Calendar.getInstance();
        dataCalendar.set(2000, Calendar.APRIL, 1, 15, 25, 10);
        Date data = dataCalendar.getTime();

        Date dateFirstHour = DateTimeUtil.getDateFirstHour(data);

        Calendar calendarFirstHour = Calendar.getInstance();
        calendarFirstHour.setTime(dateFirstHour);

        assertEquals(2000, calendarFirstHour.get(Calendar.YEAR));
        assertEquals(Calendar.APRIL, calendarFirstHour.get(Calendar.MONTH));
        assertEquals(1, calendarFirstHour.get(Calendar.DATE));

        assertEquals(0, calendarFirstHour.get(Calendar.HOUR_OF_DAY));
        assertEquals(0, calendarFirstHour.get(Calendar.MINUTE));
        assertEquals(0, calendarFirstHour.get(Calendar.SECOND));
        assertEquals(0, calendarFirstHour.get(Calendar.MILLISECOND));
    }

    @Test
    public void testGetDataUltimaHora() throws Exception {
        Calendar dataCalendar = Calendar.getInstance();
        dataCalendar.set(2000, Calendar.APRIL, 1, 15, 25, 10);
        Date data = dataCalendar.getTime();

        Date dateLastHour = DateTimeUtil.getDateLastHour(data);

        Calendar calendarLastHour = Calendar.getInstance();
        calendarLastHour.setTime(dateLastHour);

        assertEquals(2000, calendarLastHour.get(Calendar.YEAR));
        assertEquals(Calendar.APRIL, calendarLastHour.get(Calendar.MONTH));
        assertEquals(1, calendarLastHour.get(Calendar.DATE));

        assertEquals(23, calendarLastHour.get(Calendar.HOUR_OF_DAY));
        assertEquals(59, calendarLastHour.get(Calendar.MINUTE));
        assertEquals(59, calendarLastHour.get(Calendar.SECOND));
        assertEquals(999, calendarLastHour.get(Calendar.MILLISECOND));
    }
}
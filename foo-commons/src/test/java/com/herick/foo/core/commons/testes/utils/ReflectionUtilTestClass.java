package com.herick.foo.core.commons.testes.utils;

import javax.annotation.Resource;

/**
 * Classe para testes do utilitário de reflection.
 */
public class ReflectionUtilTestClass {

    @Resource
    public void metodoQualquer() {
    }

    @Resource
    public void outroMetodoQualquer() {
    }

    public void metodoNaoAnotado() {
    }

}

package com.herick.foo.core.commons.testes;

import com.herick.foo.core.utils.FooUtil;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Testes associados ao utilitário para exceções.
 */
@RunWith(JUnitParamsRunner.class)
public class RedeReceitaUtilTests {
	
	@Test
	@Parameters({
			"JOSÉ DE SOUSA, José de Sousa",
			"josé de sousa, José de Sousa",
			"papa joão paulo II, Papa João Paulo II",
	})
    public void testCapitalizarTexto(String textoOriginal, String resultadoEsperado) throws Exception{
        String resultadoObtido = FooUtil.capitalizarTexto(textoOriginal);
        assertEquals(resultadoEsperado, resultadoObtido);
    }

    @Test
    public void testCapitalizarTextoNull() throws Exception{
        String resultadoObtido = FooUtil.capitalizarTexto(null);
        assertNull(resultadoObtido);
    }

}

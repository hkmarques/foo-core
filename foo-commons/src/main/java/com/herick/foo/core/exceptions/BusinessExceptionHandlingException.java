package com.herick.foo.core.exceptions;

/**
 * Classe para lancar excecoes relativas ao tratamento customizado de exceções no negócio.
 */
public class BusinessExceptionHandlingException extends RuntimeException {

	private static final long serialVersionUID = -2721867455251151813L;

	public BusinessExceptionHandlingException() {
        super();
    }

    public BusinessExceptionHandlingException(Throwable throwable) {
        super(throwable);
    }

    public BusinessExceptionHandlingException(final String mensagem) {
        super(mensagem);
    }

    public BusinessExceptionHandlingException(final String mensagem, Throwable throwable) {
        super(mensagem, throwable);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
package com.herick.foo.core.utils;

import com.herick.foo.core.constants.StatusDatePeriod;
import java.util.Date;

/**
 * Classe utilitária para a representação de períodos de datas.
 */
public class DatePeriod {
	
	private Date startDate;
	private Date finalDate;
	
	/**
	 * Construtor do período, validando-o conforme as seguintes regras:
	 * 
	 * 1. Pelo menos uma das datas deve estar preenchida;
	 * 2. Quando ambas as datas estiverem preenchidas, o início do período deve ser anterior ao final  
	 * 
	 * @param startDate a data inicial do período
	 * @param finalDate  a data final do período
	 * @throws IllegalArgumentException quando os parâmetros para a construção do período forem inválidos
	 */
	public DatePeriod(Date startDate, Date finalDate) {
		// Valida o preenchimento de uma das datas
		if(startDate == null && finalDate == null) {
			throw new IllegalArgumentException("Favor informar um período com pelo menos uma das datas preenchida (inicial ou final)");
		}
		// Quando ambas as datas estiverem preenchidas, o início do período deve ser anterior ao final
		if(startDate != null && finalDate != null) {
			if(finalDate.before(startDate)) {
				throw new IllegalArgumentException("Favor informar um período válido (cuja data inicial seja anterior à data final)");
			}
		}
		this.startDate = startDate;
		this.finalDate = finalDate;
	}
	
	/**
	 * Verifica se a data informada como parâmetro está contida no período
	 * 
	 * ---(false)---[DataInicial]---(true)---------------------------
	 * ---(false)---[DataInicial]---(true)---[DataFinal]---(false)---
	 * -----------------------------(true)---[DataFinal]---(false)--- 
	 * 
	 * @param data a data a ser testada
	 * @return <code>true</code> caso a data esteja contida no período, <code>false</code> caso contrário (ou caso a data seja nula)
	 */
	public boolean contains(Date data) {
		return !(data == null || (this.startDate != null && data.before(this.startDate)) || (this.finalDate != null && data.after(this.finalDate)));
	}
	
	/**
	 * Verifica se este período possui intersecção com o período informado
	 * @param datePeriod o período a ser testado
	 * 
	 * @return <code>true</code> caso os períodos possuam intersecção, <code>false</code> caso contrário (ou caso o período seja nulo)
	 */
	public boolean intersects(DatePeriod datePeriod) {
		/* 
		 * Serão necessárias duas verificações, invertendo-se em cada uma o período de referência. 
		 * Ao final, combina-se os dois resultados (OR) para a detecção de quaisquer intersecções 
		 */
		return datePeriod != null && ((
			// Este período como referência
			this.contains(datePeriod.getStartDate()) || this.contains(datePeriod.getFinalDate())
		) || (
			// O período informado como referência
			datePeriod.contains(this.startDate) || datePeriod.contains(this.finalDate)
		));
	}
	
	/**
     * Retorna o status do período (Passado, Presente ou Futuro)
     * 
     * ---(Passado)---[DataInicial]---(Presente)----------------------------
     * ---(Passado)---[DataInicial]---(Presente)---[DataFinal]---(Futuro)---
     * -------------------------------(Presente)---[DataFinal]---(Futuro)--- 
     * 
     * @return StatusDataPeriodo o status do período (Passado, Presente ou Futuro)
     */
	public StatusDatePeriod getStatus() {
		StatusDatePeriod status = null;
		Date currentDateTime = DateTimeUtil.getCurrentDateTime();
		if(this.startDate != null && currentDateTime.before(this.startDate)) {
			status = StatusDatePeriod.FUTURE;
		} else if(this.finalDate != null && currentDateTime.after(this.finalDate)) {
			status = StatusDatePeriod.PAST;
		} else {																
			status = StatusDatePeriod.PRESENT;
		}
    	return status;
	}

	public Date getStartDate() {
		return startDate;
	}
	
	public Date getFinalDate() {
		return finalDate;
	}
}
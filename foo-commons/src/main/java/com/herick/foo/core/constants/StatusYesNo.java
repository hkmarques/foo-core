package com.herick.foo.core.constants;

public enum StatusYesNo {
    NO("No", 'N', false),
    YES("Yes", 'Y', true);

    private final String descricao;
    private final Character valor;
    private final boolean valorBoolean;

    StatusYesNo(String descricao, Character valor, boolean valorBoolean) {
        this.descricao = descricao;
        this.valor = valor;
        this.valorBoolean = valorBoolean;    }

    public String getDescricao() {
        return descricao;
    }

    public Character getValor() {
        return valor;
    }

    public boolean getValorBoolean() {
        return valorBoolean;
    }

    /**
     * Faz a conversão de um caractere em sua respectiva entrada no enum, caso haja uma.
     *
     * @param valor o valor a ser convertido
     * @return      o valor do enum <code>StatusSimNao</code> correspondente ao valor informado
     */
    public static StatusYesNo parse(Character valor) {
        StatusYesNo statusYesNo = null;
        for (StatusYesNo o : StatusYesNo.values()) {
            if (o.getValor() == valor) {
                statusYesNo = o;
                break;
            }
        }

        return statusYesNo;
    }

    /**
     * Faz a conversão de um boolean em sua respectiva entrada no enum, caso haja uma.
     *
     * @param valor o valor a ser convertido
     * @return      o valor do enum <code>StatusSimNao</code> correspondente ao valor informado
     */
    public static StatusYesNo parse(boolean valor) {
        return valor ? YES : NO;
    }

}

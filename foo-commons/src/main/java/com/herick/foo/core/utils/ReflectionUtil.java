package com.herick.foo.core.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe utilitária para funções e subrotinas com reflexão.
 */
public class ReflectionUtil {

    /**
     * Método para obter a partir de uma classe informada os métodos com uma anotação específica, também informada.
     *
     * @param classe          a classe em que os métodos com a anotação serão buscados
     * @param tipoAnotacao    a anotação a ser usada na busca
     * @return              a lista de métodos encontrados
     *
     * @see <a href="http://stackoverflow.com/a/6593661/965715">Fonte do método no StackOverflow</a>
     */
    public static List<Method> obterMetodosComAnotacao(final Class<?> classe, final Class<? extends Annotation> tipoAnotacao) {
        final List<Method> metodosAnotados = new ArrayList<Method>();
        Class<?> classeAtual = classe;

        while (classeAtual != Object.class) { // é necessário iterar a hierarquia para pesquisar também  os métodos herdados

            // itera a lista de métodos da classe do ponto atual na herança e adiciona os que estão anotados
            final List<Method> todosOsMetodos = new ArrayList<Method>(Arrays.asList(classeAtual.getDeclaredMethods()));
            for (final Method metodo : todosOsMetodos) {
                if (tipoAnotacao == null || metodo.isAnnotationPresent(tipoAnotacao)) {
                    metodosAnotados.add(metodo);
                }
            }

            // passa para o próximo nível na hierarquia
            classeAtual = classeAtual.getSuperclass();
        }

        return metodosAnotados;
    }

}

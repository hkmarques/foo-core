package com.herick.foo.core.utils;

import java.util.StringTokenizer;

/**
 * Classe utilitária dos sistemas Foo
 */
public class FooUtil {

	/**
	 * Método para a remoção de caracteres não numéricos de uma determinada string.
	 * @param conteudo o conteúdo que se deseja remover os caracteres não numéricos
	 * @return apenas os caracteres numéricos do conteúdo informado
	 */
	public static String removerCaracteresNaoNumericos(String conteudo) {
		return conteudo.replaceAll("[^0-9]", "");
	}
	
	/**
	 * Método para a capitalização de textos
	 * @param texto o texto a ser capitalizado
	 * @return o texto capitalizado
	 */
	public static String capitalizarTexto(String texto) {
        String resultado = null;
		final String[] ignoradas = { "e", "o", "a", "os", "as", "um", "uma", "uns", "umas", "a", "ao", "ão", "aos", "ãos", "de", "do", "da", "dos", "das", "dum", "duma", "duns", "dumas", "em", "no", "na", "nos", "nas", "num", "numa", "nuns", "numas", "por", "pelo", "pela", "pelos", "pelas", "para", "com" };
		final String[] numerosRomanos = { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X" };
		if(texto != null) {
		    final StringBuilder sb = new StringBuilder();
			final StringTokenizer st = new StringTokenizer(texto.trim(), " ", true);
			while(st.hasMoreTokens()) {
				String token = st.nextToken().toLowerCase();
				if(!token.trim().equals("")) {
					token = String.format("%s%s", Character.toUpperCase(token.charAt(0)), token.substring(1));
					for(String ignorada : ignoradas) {
						if(token.equalsIgnoreCase(ignorada)) {
							token = String.format("%s%s", Character.toLowerCase(token.charAt(0)), token.substring(1));
							break;
						}
					}
					for(String numero : numerosRomanos) {
						if(token.equalsIgnoreCase(numero)) {
							token = token.toUpperCase();
							break;
						}
					}
				}
				sb.append(token);
			}
		    resultado = sb.toString();
		}
        return resultado;
	}
}

package com.herick.foo.core.messages;

import com.herick.foo.core.constants.FooConstants;
import org.springframework.stereotype.Component;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

@Component
public class FooMessages {
	
	// Arquivo de mensagens padrão a ser utilizado pelo sistema Foo Core (i18n)
	ResourceBundle arquivoMensagensPadrao;

	// Lista de arquivos contendo as mensagens a serem utilizadas pelo sistema Foo Core (i18n)
	private LinkedList<ResourceBundle> listaArquivosMensagens;

    public FooMessages() {
    	// Inicializa a lista de arquivos de mensagens, adicionando arquivo padrão (mensagens básicas)
    	arquivoMensagensPadrao = ResourceBundle.getBundle(FooConstants.MESSAGE_RESOURCES_FILE);
    	this.listaArquivosMensagens = new LinkedList<ResourceBundle>();
    	this.listaArquivosMensagens.add(arquivoMensagensPadrao);
	}

    /**
     * Método para traduzir uma mensagem no arquivo de i18n a partir de sua chave.
     *
     * @param mensagemChave chave da mensagem a ser traduzida (deve estar contida no arquivo de mensagens i18n)
     * @return mensagem traduzida
     */
    public String getTranslatedMessage(String mensagemChave) throws MissingResourceException {
        String mensagemTraduzida = "";
        // Varre todos os arquivos de messages em busca da chave (começando pelos arquivos customizados, na sequência dos arquivos adicionados)
        Iterator<ResourceBundle> listaArquivosMensagensIterator = this.listaArquivosMensagens.iterator();

        while (listaArquivosMensagensIterator.hasNext()) {
            try {
                ResourceBundle resourceCorrente = listaArquivosMensagensIterator.next();
                mensagemTraduzida = resourceCorrente.getObject(mensagemChave).toString();
                break;
            } catch (MissingResourceException mre) {
                if(!listaArquivosMensagensIterator.hasNext()) {
                    // Caso seja o último arquivo disponível na lista e a mensagem não tiver sido encontrada, lança exceção
                    throw new MissingResourceException(arquivoMensagensPadrao.getObject("mensagens_traducao_erro_chave").toString() + " [ " + mensagemChave + " ]", "", mensagemChave);
                }
            }
        }
        return mensagemTraduzida;
    }

    /**
     * Método para traduzir uma mensagem no arquivo de i18n a partir de sua chave, usando parâmetros a serem substituídos.
     *
     * @param mensagemChave         chave da mensagem a ser traduzida (deve estar contida no arquivo de mensagens i18n)
     * @param mensagemParametros    os parâmetros a serem substituídos na mensagem final
     * @return mensagem traduzida
     */
    public String getTranslatedMessage(String mensagemChave, Object... mensagemParametros) throws MissingResourceException {
        String mensagemTraduzida = this.getTranslatedMessage(mensagemChave);
        return MessageFormat.format(mensagemTraduzida, mensagemParametros);
    }

    /**
     * Método para adicionar arquivos de mensagens customizadas a serem utilizadas pelo sistema Foo Core (i18n).
     * Os arquivos são sempre adicionados no início da lista para que a mesma se comporte como uma pilha (LIFO).
     * Obs.: Implementação de pilha do Java não foi utilizada pois a mesma, quando iterada, retorna os elementos na ordem FIFO.
     * @see <a href="http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4475301">iterator() iterates the wrong way</a>
     *
     * @param arquivoMensagens caminho do arquivo de mensagens a ser adicionado.
     * @throws MissingResourceException exceção a ser lançada caso o arquivo de mensagens passado como parâmetro não exista.
     */
    public void adicionarArquivoMensagens(String arquivoMensagens) throws MissingResourceException {
    	this.listaArquivosMensagens.addFirst(ResourceBundle.getBundle(arquivoMensagens));
    }
}

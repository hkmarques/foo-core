package com.herick.foo.core.constants;

public enum StatusDatePeriod {
    PAST("Past"),
    PRESENT("Present"),
    FUTURE("Future");

    private String description;

    StatusDatePeriod(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}

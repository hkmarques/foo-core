package com.herick.foo.core.constants;


/**
 * Classe de constantes do módulo Foo Core.
 * Armazenamento de parâmetros fixos a serem utilizados durante a codificação.
 */
public class FooConstants {

	/** Arquivo contendo as mensagens associadas ao Foo Core */
	public static final String MESSAGE_RESOURCES_FILE = "com.herick.foo.core.messages.MessageResources";
}
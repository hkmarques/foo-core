package com.herick.foo.core.utils;

import org.joda.time.DateTime;
import java.util.Date;

/**
 * Classe utilitária para operações envolvendo data / hora.
 */
public class DateTimeUtil {

    /**
     * Retorna a data/hora atuais do servidor.
     *
     * @return a data/hora atuais do servidor
     */
    public static Date getCurrentDateTime() {
        return new DateTime().toDate();
    }

    /**
     * Retorna a data atual do servidor.
     *
     * @return a data atual do servidor
     */
    public static Date getCurrentDate() {
        DateTime dataAtual = new DateTime();
        return getDateFirstHour(dataAtual.toDate());
    }

    /**
     * Retorna a data informada, com a primeira hora do dia.
     *
     * @param data a data cuja hora será alterada
     * @return a data informada, com a primeira hora do dia
     */
    public static Date getDateFirstHour(Date data) {
        DateTime dataPrimeiraHora = new DateTime(data.getTime()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
        return dataPrimeiraHora.toDate();
    }

    /**
     * Retorna a data informada, com a última hora do dia.
     *
     * @param data a data cuja hora será alterada
     * @return a data informada, com a última hora do dia
     */
    public static Date getDateLastHour(Date data) {
        DateTime dataUltimaHora = new DateTime(data.getTime()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);
        return dataUltimaHora.toDate();
    }
}

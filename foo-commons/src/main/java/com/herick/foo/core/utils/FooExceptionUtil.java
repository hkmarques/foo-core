package com.herick.foo.core.utils;


/**
 * Classe utilitária para tratamento de exceções da aplicação.
 */
public class FooExceptionUtil {
	
	/**
	 * Método responsável por extrair as mensagens de exceção em todos os níveis existentes (Cause)
	 * 
	 * @param throwable exceção de onde se deseja extrair a mensagem
	 * @return mensagem completa da exceção
	 */
	public static String extractExceptionMessage(Throwable throwable) {
		StringBuilder msgExcecao = new StringBuilder();
		if(throwable != null) {
			msgExcecao.append(throwable.getMessage() != null ? throwable.getMessage() : "NullPointerException");
		}
		Throwable cause = throwable;
		while(cause.getCause() != null) {
			cause = cause.getCause();
			msgExcecao.append(" - ").append(cause.getMessage() != null ? cause.getMessage() : "NullPointerException");
		}
		return msgExcecao.toString();
	}

	/**
	 * Processa um trecho de stacktrace informado destacando as ocorrências textuais dos pacotes corporativos internos.
	 *
	 * @param stacktrace    o stacktrace a ser formatado
	 * @return				o stacktrace informado, após aplicar formatação especial às ocorrências textuais dos pacotes corporativos internos
	 */
	public static String formatStacktrace(String stacktrace) {
		// Expressão regular para buscar todas as ocorrências (linhas) relacionadas a classes da (...) dentro do stacktrace
		// (?m)	- modo multi-linhas
		// ^	- início de linha
		// .*	- qualquer combinação de caracteres (ou nenhum caractere)
		// (|)	- ocorrência de um ou outro termo (pacote)
		// $	- fim de linha
		String expressaoRegularFormatador = "(?m)^.*(com.herick).*$";
		return stacktrace.replaceAll(expressaoRegularFormatador, "<span class=\"panel-erros-conteudo-destaque\">$0</span>");
	}
}
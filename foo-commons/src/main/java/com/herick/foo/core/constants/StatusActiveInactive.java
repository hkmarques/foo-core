package com.herick.foo.core.constants;

/**
 * Enum usado na aplicação para indicar o status associado a determinados conceitos.
 */
public enum StatusActiveInactive {
	ACTIVE("Active"),
	INACTIVE("Inactive");

	private String description;

	StatusActiveInactive(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
package com.herick.foo.core.exceptions.aspects;

import com.herick.foo.core.exceptions.BusinessExceptionHandlingException;
import com.herick.foo.core.exceptions.annotations.ExceptionHandling;
import com.herick.foo.core.messages.FooMessages;
import com.herick.foo.core.utils.FooExceptionUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import java.lang.reflect.ParameterizedType;

@Aspect
@Component
@EnableAspectJAutoProxy
@SuppressWarnings("rawtypes")
public class BusinessExceptionHandlingAspect implements Ordered {

	@Autowired
    FooMessages fooMessages;
	
    /**
     * Advice responsável pelo tratamento de exceções nos métodos com a anotação Transactional.
     *
     * @param joinPoint                                 o contexto da operação a ser executada
     * @param exceptionHandling                        limita os métodos influenciados pela anotação NegocioExcecoesTratamento
     * @throws BusinessExceptionHandlingException       caso a operação influenciada falhe
     */
	@Around("@annotation(exceptionHandling)")
    private Object handleException(ProceedingJoinPoint joinPoint, ExceptionHandling exceptionHandling) throws BusinessExceptionHandlingException {
        try {
            return joinPoint.proceed();
        } catch (Throwable throwable) {
        	// Extrai informações do método e da entidade para montar mensagem de erro
            Signature methodSignature = joinPoint.getSignature();
            ParameterizedType genericInterface = (ParameterizedType) joinPoint.getTarget().getClass().getGenericSuperclass();
            final String entityName = ((Class)genericInterface.getActualTypeArguments()[0]).getSimpleName();
            final String methodName = methodSignature.getName();
            // Monta mensagem de erro particularizada para cada entidade e método invocados
            String msg = fooMessages.getTranslatedMessage("negocio_aspecto_erro", methodName, entityName, FooExceptionUtil.extractExceptionMessage(throwable));
            throw new BusinessExceptionHandlingException(msg, throwable);
        }
    }

    /**
     * Determina que este aspecto terá prioridade sobre os demais (e.g. escopo da transação), ou
     * seja, que os advices aqui definidos englobarão o contexto dos demais aspectos e estarão portanto
     * aptos a interceptar exceções lançadas em contextos mais internos.
     */
    @Override
    public int getOrder() {
        return 10;
    }
}
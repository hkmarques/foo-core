package com.herick.foo.core.utils.aspects;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

@Aspect
@Component
@EnableAspectJAutoProxy
public class BusinessLoggingAspect implements Ordered {

	Logger logger = Logger.getLogger(BusinessLoggingAspect.class);
	
    /**
     * Advice responsável pelo logging de qualquer operação de negócio realizada no sistema.
     */
	@Before("within(com.herick..business..*)")
	private void logBusinessOperation(JoinPoint joinpoint) {
		// Logar caso o nível definido para a aplicação seja mais verboso do que INFO (DEBUG, TRACE ou ALL)
		if(!Logger.getRootLogger().getLevel().isGreaterOrEqual(Level.INFO)) {
			logger.debug(":::::::::::: " + joinpoint.getTarget().getClass().getName() + "." + joinpoint.getSignature().getName());
		}
	}
	
	/**
	 * Advice responsável pelo logging de operações nas fachadas do sistema.
	 */
	@Before("within(com.herick..business.facades..*)")
    private void logOperacaoNegocioFachada(JoinPoint joinpoint) {
		// Logar caso o nível definido para a aplicação seja menos verboso ou igual a INFO (INFO, WARN, ERROR, FATAL)
		if(Logger.getRootLogger().getLevel().isGreaterOrEqual(Level.INFO)) {
			logger.info(":::::::::::: " + joinpoint.getTarget().getClass().getName() + "." + joinpoint.getSignature().getName());
		}
    }
	
    @Override
    public int getOrder() {
        return 100;
    }
}
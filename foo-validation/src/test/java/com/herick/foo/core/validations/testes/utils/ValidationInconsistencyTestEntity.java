package com.herick.foo.core.validations.testes.utils;

import com.herick.foo.core.messages.FooMessages;
import com.herick.foo.core.validations.ValidationInconsistency;

/**
 * Implementação interna para testes da classe para inconsistências de validação.
 */
public class ValidationInconsistencyTestEntity extends ValidationInconsistency {

    /**
     * Instancia <code>ValidationInconsistency</code> inicializando os campos básicos.
     *
     * @param propriedade        o nome da propriedade associada à inconsistência
     * @param mensagemChave      a chave da mensagem associada à inconsistência
     * @param mensagemParametros os parâmetros a serem substituídos na mensagem final associada à inconsistência
     * @see ValidationInconsistency
     */
    public ValidationInconsistencyTestEntity(String propriedade, String mensagemChave, String... mensagemParametros) {
        super(propriedade, mensagemChave, mensagemParametros);
    }

    /**
     * Provedor de mensagens neste caso não pesquisa mensagens usadas nos testes em nenhum arquivo externo.
     *
     * @return uma implementação interna do provedor de mensagens, apenas para testes
     */
    @Override
    public FooMessages getProvedorMensagens() {
        return new FooMessages();
    }

}

package com.herick.foo.core.validations.testes.exceptions;

import com.herick.foo.core.validations.ValidationInconsistency;
import com.herick.foo.core.validations.excecoes.BusinessValidationException;
import com.herick.foo.core.validations.testes.utils.ValidationInconsistencyTestEntity;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;

/**
 * Testes associados à exceção para operações rejeitadas pelas validações.
 */
public class BusinessValidationExceptionTests {

    @Test
    public void testGetMessage() throws Exception {

        ValidationInconsistency inconsistencia1 = new ValidationInconsistencyTestEntity("propriedadeQualquer", "inconsistencia1");
        ValidationInconsistency inconsistencia2 = new ValidationInconsistencyTestEntity("outraPropriedade", "inconsistencia2");
        ValidationInconsistency inconsistencia3 = new ValidationInconsistencyTestEntity("maisUmaPropriedade", "inconsistencia3");
        BusinessValidationException excecao = new BusinessValidationException("Mensagem geral da exceção", Arrays.asList(inconsistencia1, inconsistencia2, inconsistencia3));

        String mensagemEsperada = "Mensagem geral da exceção - propriedadeQualquer: Inconsistencia 1; outraPropriedade: Inconsistencia 2; maisUmaPropriedade: Inconsistencia 3";

        assertEquals("A mensagem obtida é diferente da esperada. ", mensagemEsperada, excecao.getMessage());
    }

}

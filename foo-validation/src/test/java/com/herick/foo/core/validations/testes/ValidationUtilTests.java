package com.herick.foo.core.validations.testes;

import com.herick.foo.core.validations.utils.ValidationUtil;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.fluttercode.datafactory.impl.DataFactory;
import org.joda.time.DateTimeUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import java.util.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Testes associados à classe de métodos utilitários de validação.
 */
@RunWith(JUnitParamsRunner.class)
public class ValidationUtilTests {

    // region # Casos inválidos

    @Test
    public void textoObrigatorioNulo() throws Exception {
        String texto = null;
        assertFalse(ValidationUtil.validateRequiredText(texto));
    }

    @Test
    public void textoObrigatorioVazio() throws Exception {
        String texto = "";
        assertFalse(ValidationUtil.validateRequiredText(texto));
    }
    
    @Test
    public void textoObrigatorioEspacos() throws Exception {
        String texto = "     ";
        assertFalse(ValidationUtil.validateRequiredText(texto));
    }
    
    @Test
    public void caractereObrigatorioNulo() throws Exception {
        Character caractere = null;
        assertFalse(ValidationUtil.validateRequiredCharacter(caractere));
    }

    @Test
    public void caractereObrigatorioVazio() throws Exception {
    	Character caractere = Character.valueOf('\0');
        assertFalse(ValidationUtil.validateRequiredCharacter(caractere));
    }

    @Test
    public void campoPreenchimentoNaoPermitidoPreenchido() throws Exception {
        Object valor = new Date();
        assertFalse(ValidationUtil.validateFieldShouldBeEmpty(valor));
    }

    @Test
    public void campoObrigatorioNulo() throws Exception {
        Object objeto = null;
        assertFalse(ValidationUtil.validateRequiredField(objeto));
    }

    @Test
    public void listaObrigatoriaNula() throws Exception {
        List<Integer> lista = null;
        assertFalse(ValidationUtil.validateRequiredList(lista));
    }

    @Test
    public void listaObrigatoriaVazia() throws Exception {
        List<Integer> lista = new ArrayList<Integer>();
        assertFalse(ValidationUtil.validateRequiredList(lista));
    }

    @Test
    public void periodoDataFinalAnteriorDataInicial() throws Exception {
        Calendar dataFinal = Calendar.getInstance();
        dataFinal.set(2000, Calendar.JANUARY, 1);

        Calendar dataInicial = Calendar.getInstance();
        dataInicial.set(2001, Calendar.DECEMBER, 31);

        assertFalse("A validação deveria rejeitar o período com data final anterior à inicial!", ValidationUtil.validatePeriod(dataInicial.getTime(), dataFinal.getTime()));
    }

    @Test
    public void dataPassadaInvalida() throws Exception {
        Calendar dataAtual = Calendar.getInstance();
        dataAtual.set(2014, Calendar.JANUARY, 1);
        DateTimeUtils.setCurrentMillisFixed(dataAtual.getTimeInMillis());

        Calendar dataPassada = Calendar.getInstance();
        dataPassada.set(2001, Calendar.DECEMBER, 31);

        assertFalse("A validação deveria rejeitar a data anterior à atual!", ValidationUtil.validateDateNonRetroactive(dataPassada.getTime()));
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void dataHoraPassadaInvalida() throws Exception {
        Calendar dataAtual = Calendar.getInstance();
        dataAtual.set(2014, Calendar.JANUARY, 1, 0, 0, 2);              // Data/hora atual = 01/01/2014 hora: forçando 00:00:02
        DateTimeUtils.setCurrentMillisFixed(dataAtual.getTimeInMillis());

        Calendar dataPassada = Calendar.getInstance();
        dataPassada.set(1900, Calendar.DECEMBER, 31);                    // Data/hora testada = 31/12/1900 hora: instante de execução do teste

        assertFalse("A validação deveria rejeitar a data/hora anterior à atual!", ValidationUtil.validateDateTimeNonRetroactive(dataPassada.getTime()));

        Calendar dataHoraFutura = Calendar.getInstance();
        dataHoraFutura.set(2014, Calendar.JANUARY, 1, 0, 0, 1);         // Data/hora testada = 01/01/2014 hora: forçando 00:00:01
        assertFalse("A validação deveria rejeitar a data/hora atual com hora anterior!", ValidationUtil.validateDateTimeNonRetroactive(dataHoraFutura.getTime()));

        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void dataFuturaInvalida() throws Exception {
        Calendar dataAtual = Calendar.getInstance();
        dataAtual.set(2014, Calendar.JANUARY, 1);
        DateTimeUtils.setCurrentMillisFixed(dataAtual.getTimeInMillis());

        Calendar dataFutura = Calendar.getInstance();
        dataFutura.set(2121, Calendar.DECEMBER, 31);

        assertFalse("A validação deveria rejeitar a data posterior à atual!", ValidationUtil.validateDateNonFuture(dataFutura.getTime()));
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void dataHoraFuturaInvalida() throws Exception {
        Calendar dataAtual = Calendar.getInstance();
        dataAtual.set(2014, Calendar.JANUARY, 1, 0, 0, 1);              // Data/hora atual = 01/01/2014 hora: forçando 00:00:01
        DateTimeUtils.setCurrentMillisFixed(dataAtual.getTimeInMillis());

        Calendar dataFutura = Calendar.getInstance();
        dataFutura.set(2124, Calendar.DECEMBER, 31);                    // Data/hora testada = 31/12/2124 hora: instante de execução do teste

        assertFalse("A validação deveria rejeitar a data/hora posterior à atual!", ValidationUtil.validateDateTimeNonFuture(dataFutura.getTime()));

        Calendar dataHoraFutura = Calendar.getInstance();
        dataHoraFutura.set(2014, Calendar.JANUARY, 1, 0, 0, 2);         // Data/hora testada = 01/01/2014 hora: forçando 00:00:02
        assertFalse("A validação deveria rejeitar a data/hora atual com hora posterior!", ValidationUtil.validateDateTimeNonFuture(dataHoraFutura.getTime()));

        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    @Parameters({
        "email",
        "email.com",
        "email.com.br",
        "email_server.com.br",
        "@server.com",
        "@server.com.br",
        "\\@server.com.br",
        ".@server.com.br",
        ".@.",
        "@",
        "www.server.com",
        "http://www.server.com",
        "mailto:email@server.com",
        "email*server.com",
        "email com whitespace@server.com",
        "email_com_ç@server.com",
        "email_com_ã@server.com" })
    public void erroEmailInvalido(String email) throws Exception {
        assertFalse("O método deveria rejeitar o email informado: " + email, ValidationUtil.validateEmail(email));
    }

    @Test
    @Parameters({
            "0, 1",
            "5, 6",
            "100, 101",
    })
    public void erroTextoComprimentoNaoPermitido(int quantidadeCaracteresPermitida, int quantidadeCaracteresInformada) throws Exception {
        DataFactory dataFactory = new DataFactory();
        String texto = dataFactory.getRandomText(quantidadeCaracteresInformada);
        assertFalse("O método deveria rejeitar o texto. Tamanho máximo permitido: " + quantidadeCaracteresPermitida + " Tamanho do texto informado: " + quantidadeCaracteresInformada, ValidationUtil.validateTextCharacterAmount(quantidadeCaracteresPermitida, texto));
    }

    @Test
    @Parameters({
            "0, 1",
            "5, 4",
            "100, 101",
            "100, 99"
    })
    public void erroTextoComprimentoDiferente(int quantidadeCaracteresPermitida, int quantidadeCaracteresInformada) throws Exception {
        DataFactory dataFactory = new DataFactory();
        String texto = dataFactory.getRandomText(quantidadeCaracteresInformada);
        assertFalse("O método deveria rejeitar o texto. Tamanho exato permitido: " + quantidadeCaracteresPermitida + " Tamanho do texto informado: " + quantidadeCaracteresInformada, ValidationUtil.validateTextCharacterExactAmount(quantidadeCaracteresPermitida, texto));
    }

    @Test
    public void erroValorForaDominioPermitidoInteger() throws Exception {
        int[] dominio = {1, 2, 3};
        int valor = 4;
        assertFalse("O método deveria rejeitar o valor. Domínio permitido: " + dominio + " Valor informado: " + valor, ValidationUtil.validateDomainPermittedValue(dominio, valor));

        int[] dominio2 = {654, 35, 98};
        valor = 1;
        assertFalse("O método deveria rejeitar o valor. Domínio permitido: " + dominio2 + " Valor informado: " + valor, ValidationUtil.validateDomainPermittedValue(dominio2, valor));
    }

    @Test
    public void erroValorForaDominioPermitidoCharacter() throws Exception {
        Character[] dominio = {'a', 'b', 'c'};
        Character valor = 'd';
        assertFalse("O método deveria rejeitar o valor. Domínio permitido: " + dominio + " Valor informado: " + valor, ValidationUtil.validateDomainPermittedValue(dominio, valor));

        Character[] dominio2 = {'H', 'U', 'T'};
        valor = 'K';
        assertFalse("O método deveria rejeitar o valor. Domínio permitido: " + dominio2 + " Valor informado: " + valor, ValidationUtil.validateDomainPermittedValue(dominio2, valor));
    }

    @Test
    @Parameters({
            "00000000000",
            "11111111111",
            "22222222222",
            "33333333333",
            "44444444444",
            "55555555555",
            "66666666666",
            "77777777777",
            "88888888888",
            "99999999999",
            "12345678900",
            "98765432132"
    })
    public void testErroCpfInvalido(String cpf) throws Exception {
        assertFalse("A validação deveria rejeitar o CPF inválido: " + cpf, ValidationUtil.validateCpf(cpf));
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters({
            "979.58577117",
            "979.585.771+.++.17",
            "979.585...771-17",
            "979..585...771-17",
    })
    public void testErroCpfInvalidoExcecao(String cpf) throws Exception {
        ValidationUtil.validateCpf(cpf);
    }

    @Test
    @Parameters({
            "00000000000000",
            "11111111111111",
            "22222222222222",
            "33333333333333",
            "44444444444444",
            "55555555555555",
            "66666666666666",
            "77777777777777",
            "88888888888888",
            "99999999999999",
            "35431219879564",
            "35.431.219/8795-64"
    })
    public void testErroCnpjInvalido(String cnpj) throws Exception {
        assertFalse("A validação deveria rejeitar o CNPJ inválido: " + cnpj, ValidationUtil.validateCnpj(cnpj));
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters({
            "35.236.808/001-57",
            "35.236.808//0001-57",
            "35.236.808/0001--57",
            "35.236.808/000157",
            "35.236.808000157",
            "35.236.808++-.+-000157",
    })
    public void testErroCnpjInvalidoExcecao(String cnpj) throws Exception {
        ValidationUtil.validateCnpj(cnpj);
    }

    @Test
    @Parameters({
            "1, 7, 0",
            "5, 255, 256",
            "654, 32135, 560531"
    })
    public void testErroValorForaIntervaloPermitido(Integer intervaloInicio, Integer intervaloFim, Integer valor) throws Exception {
        String mensagemErro = "O método deveria rejeitar o valor. Intervalo permitido: [" + intervaloInicio + "-" + intervaloFim + "], Valor informado: " + valor;
        assertFalse(mensagemErro, ValidationUtil.validateValuePermittedInterval(intervaloInicio, intervaloFim, valor));
    }

    // endregion

    // region # Casos válidos

    @Test
    public void textoObrigatorioPreenchido() throws Exception {
        String texto = "string qualquer";
        assertTrue(ValidationUtil.validateRequiredText(texto));
    }

    @Test
    public void campoObrigatorioPreenchido() throws Exception {
        Object objeto = new Object();
        assertTrue(ValidationUtil.validateRequiredField(objeto));
    }

    @Test
    public void campoPreenchimentoNaoPermitidoNulo() throws Exception {
        Object objeto = null;
        assertTrue(ValidationUtil.validateFieldShouldBeEmpty(objeto));
    }

    @Test
    public void listaObrigatoriaPreenchida() throws Exception {
        List<Integer> lista = Arrays.asList(1, 2, 3);
        assertTrue(ValidationUtil.validateRequiredList(lista));
    }

    @Test
    @Parameters({
            "teste@teste.com",
            "teste@teste.com.br",
            "teste@bla.net",
            "email_com_underscore@bla.net",
            "numeros123f@fulano-bla.com",
            "123_a.sdf@fulano-bla.com",
            "email-com-hifen@fulano-bla.com"
    })
    public void emailValido(String email) throws Exception {
        assertTrue("O método deveria aceitar o email informado: " + email, ValidationUtil.validateEmail(email));
    }

    @Test
    public void periodoUmDia() throws Exception {
        Calendar dataFinal = Calendar.getInstance();
        dataFinal.set(2000, Calendar.JANUARY, 1, 0, 0, 1);

        Calendar dataInicial = Calendar.getInstance();
        dataInicial.set(2000, Calendar.JANUARY, 1, 0, 0, 0);

        assertTrue("A validação deveria aceitar o período de um dia!", ValidationUtil.validatePeriod(dataInicial.getTime(), dataFinal.getTime()));
    }

    @Test
    public void dataNaoRetroativaValida() throws Exception {
        Calendar dataAtual = Calendar.getInstance();
        dataAtual.set(2014, Calendar.JANUARY, 1);                           // Data atual (referência) = 01/01/2014
        DateTimeUtils.setCurrentMillisFixed(dataAtual.getTimeInMillis());

        Calendar dataFutura = Calendar.getInstance();
        dataFutura.set(2121, Calendar.DECEMBER, 31);                        // Data testada = 31/12/2121

        assertTrue("A validação deveria aprovar a data posterior à atual!", ValidationUtil.validateDateNonRetroactive(dataFutura.getTime()));
        assertTrue("A validação deveria aprovar a data atual!", ValidationUtil.validateDateNonRetroactive(dataAtual.getTime()));

        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void dataHoraNaoRetroativaValida() throws Exception {
        Calendar dataAtual = Calendar.getInstance();
        dataAtual.set(2014, Calendar.JANUARY, 1);                           // Data/hora atual (referência) = 01/01/2014 hora: instante de execução do teste
        DateTimeUtils.setCurrentMillisFixed(dataAtual.getTimeInMillis());

        Calendar dataFutura = Calendar.getInstance();
        dataFutura.set(2121, Calendar.DECEMBER, 31);                        // Data/hora testada = 31/12/2121 hora: instante de execução do teste

        assertTrue("A validação deveria aprovar a data/hora posterior à atual!", ValidationUtil.validateDateTimeNonRetroactive(dataFutura.getTime()));
        assertTrue("A validação deveria aprovar a data/hora atual com a mesma hora!", ValidationUtil.validateDateTimeNonRetroactive(dataAtual.getTime()));

        Calendar dataHoraFutura = Calendar.getInstance();
        dataHoraFutura.set(2014, Calendar.JANUARY, 1, 23, 59, 59);          // Data/hora testada = 31/12/2121 hora: forçando 23:59:59
        assertTrue("A validação deveria aprovar a data/hora atual com hora posterior!", ValidationUtil.validateDateTimeNonRetroactive(dataHoraFutura.getTime()));

        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void dataNaoFuturaValida() throws Exception {
        Calendar dataAtual = Calendar.getInstance();
        dataAtual.set(2014, Calendar.JANUARY, 1);                                       // Data atual (referência) = 01/01/2014
        DateTimeUtils.setCurrentMillisFixed(dataAtual.getTimeInMillis());

        Calendar dataPassada = Calendar.getInstance();
        dataPassada.set(1900, Calendar.DECEMBER, 31);                                   // Data testada = 31/12/1900

        assertTrue("A validação deveria aprovar a data anterior à atual!", ValidationUtil.validateDateNonFuture(dataPassada.getTime()));
        assertTrue("A validação deveria aprovar a data atual!", ValidationUtil.validateDateNonFuture(dataAtual.getTime()));

        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void dataHoraNaoFuturaValida() throws Exception {
        Calendar dataAtual = Calendar.getInstance();
        dataAtual.set(2014, Calendar.JANUARY, 1, 0, 0, 2);              // Data/hora atual = 01/01/2014 hora: forçando 00:00:02
        DateTimeUtils.setCurrentMillisFixed(dataAtual.getTimeInMillis());

        Calendar dataPassada = Calendar.getInstance();
        dataPassada.set(1900, Calendar.DECEMBER, 31);                   // Data/hora testada = 31/12/1900 hora: instante de execução do teste

        assertTrue("A validação deveria aprovar a data/hora anterior à atual!", ValidationUtil.validateDateTimeNonFuture(dataPassada.getTime()));
        assertTrue("A validação deveria aprovar a data/hora atual com a mesma hora!", ValidationUtil.validateDateTimeNonFuture(dataAtual.getTime()));

        Calendar dataHoraFutura = Calendar.getInstance();
        dataHoraFutura.set(2014, Calendar.JANUARY, 1, 0, 0, 1);          // Data/hora testada = 01/01/2014 hora: forçando 00:00:01
        assertTrue("A validação deveria aprovar a data/hora atual com hora anterior!", ValidationUtil.validateDateTimeNonFuture(dataHoraFutura.getTime()));

        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    @Parameters({
            "0",
            "5",
            "100"
    })
    public void textoComprimentoPermitido(int quantidadeCaracteresPermitida) throws Exception {
        DataFactory dataFactory = new DataFactory();
        String texto = dataFactory.getRandomText(quantidadeCaracteresPermitida);
        assertTrue("A validação deveria aceitar o texto com quantidade permitida de caracteres!", ValidationUtil.validateTextCharacterAmount(quantidadeCaracteresPermitida, texto));
    }

    @Test
    @Parameters({
            "0",
            "5",
            "100"
    })
    public void textoComprimentoExato(int quantidadeCaracteresPermitida) throws Exception {
        DataFactory dataFactory = new DataFactory();
        String texto = dataFactory.getRandomText(quantidadeCaracteresPermitida);
        assertTrue("A validação deveria aceitar o texto com quantidade exata permitida de caracteres!", ValidationUtil.validateTextCharacterExactAmount(quantidadeCaracteresPermitida, texto));
    }

    @Test
    @Parameters({
            "54515772937",
            "32539557961",
            "97958577117",
            "979.585.771-17"
    })
    public void testValidacaoCpf(String cpf) throws Exception {
        assertTrue("A validação deveria aprovar o CPF informado: " + cpf, ValidationUtil.validateCpf(cpf));
    }

    @Test
    @Parameters({
            "34430229000188",
            "67958717000111",
            "35236808000157",
            "35.236.808/0001-57",
    })
    public void testValidacaoCnpj(String cnpj) throws Exception {
        assertTrue("A validação deveria aprovar o CNPJ informado: " + cnpj, ValidationUtil.validateCnpj(cnpj));
    }

    @Test
    public void valorDentroDominioPermitidoInteger() throws Exception {
        int[] dominio = {1, 2, 3};
        int valor = 1;
        assertTrue("O método deveria aceitar o valor. Domínio permitido: " + dominio + " Valor informado: " + valor, ValidationUtil.validateDomainPermittedValue(dominio, valor));

        int[] dominio2 = {654, 35, 98};
        valor = 98;
        assertTrue("O método deveria aceitar o valor. Domínio permitido: " + dominio2 + " Valor informado: " + valor, ValidationUtil.validateDomainPermittedValue(dominio2, valor));
    }

    @Test
    public void valorDentroDominioPermitidoCharacter() throws Exception {
        Character[] dominio = {'a', 'b', 'c'};
        Character valor = 'a';
        assertTrue("O método deveria aceitar o valor. Domínio permitido: " + dominio + " Valor informado: " + valor, ValidationUtil.validateDomainPermittedValue(dominio, valor));

        Character[] dominio2 = {'G', 'H', 'K'};
        valor = 'H';
        assertTrue("O método deveria aceitar o valor. Domínio permitido: " + dominio2 + " Valor informado: " + valor, ValidationUtil.validateDomainPermittedValue(dominio2, valor));
    }

    @Test
    @Parameters({
            "1, 7, 1",
            "5, 255, 255",
            "654, 32135, 800"
    })
    public void valorDentroIntervaloPermitidoInteger(Integer intervaloInicio, Integer intervaloFim, Integer valor) throws Exception {
        String mensagemErro = "O método deveria aceitar o valor. Intervalo permitido: [" + intervaloInicio + "-" + intervaloFim + "], Valor informado: " + valor;
        assertTrue(mensagemErro, ValidationUtil.validateValuePermittedInterval(intervaloInicio, intervaloFim, valor));
    }

    // endregion
}

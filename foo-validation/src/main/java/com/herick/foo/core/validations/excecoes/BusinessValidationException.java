package com.herick.foo.core.validations.excecoes;

import com.herick.foo.core.validations.ValidationInconsistency;
import java.util.List;

/**
 * Classe para lançar exceções relacionadas às validações do negócio.
 */
public class BusinessValidationException extends RuntimeException {

    private static final long serialVersionUID = 3199048510123069431L;

    private String mensagemResumidaInconsistencias;
    private List<ValidationInconsistency> listaInconsistencias;

    public BusinessValidationException() {
        super();
    }

    public BusinessValidationException(Throwable throwable) {
        super(throwable);
        this.mensagemResumidaInconsistencias = throwable.getMessage();
    }

    public BusinessValidationException(final String mensagem) {
        super(mensagem);
        this.mensagemResumidaInconsistencias = mensagem;
    }

    public BusinessValidationException(final String mensagem, Throwable throwable) {
        super(mensagem, throwable);
        this.mensagemResumidaInconsistencias = mensagem;
    }

    public BusinessValidationException(Throwable throwable, List<ValidationInconsistency> listaInconsistencias) {
        super(throwable);
        this.mensagemResumidaInconsistencias = throwable.getMessage();
        this.listaInconsistencias = listaInconsistencias;
    }

    public BusinessValidationException(final String mensagem, List<ValidationInconsistency> listaInconsistencias) {
        super(mensagem);
        this.mensagemResumidaInconsistencias = mensagem;
        this.listaInconsistencias = listaInconsistencias;
    }

    public BusinessValidationException(final String mensagem, Throwable throwable, List<ValidationInconsistency> listaInconsistencias) {
        super(mensagem, throwable);
        this.mensagemResumidaInconsistencias = mensagem;
        this.listaInconsistencias = listaInconsistencias;
    }

    public List<ValidationInconsistency> getListaInconsistencias() {
        return listaInconsistencias;
    }

    @Override
    public String getMessage() {
        StringBuilder builder = new StringBuilder(super.getMessage());
        builder.append(" - ");
        for (ValidationInconsistency inconsistencia : this.listaInconsistencias) {
            builder.append(inconsistencia.getPropriedade())
                    .append(": ")
                    .append(inconsistencia.getMensagem())
                    .append("; ");
        }

        String mensagem = builder.toString();
        return mensagem.substring(0, mensagem.lastIndexOf(";"));
    }

	public String getMensagemResumidaInconsistencias() {
		return mensagemResumidaInconsistencias;
	}
}
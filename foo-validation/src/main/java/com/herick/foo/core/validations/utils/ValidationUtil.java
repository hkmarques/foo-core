package com.herick.foo.core.validations.utils;

import com.herick.foo.core.utils.DateTimeUtil;
import org.apache.commons.validator.routines.EmailValidator;
import java.util.Date;
import java.util.List;

/**
 * Reúne métodos utilitários relacionados às validações.
 */
public class ValidationUtil {

    /**
     * Método responsável por testar se um campo foi preenchido.
     *
     * @param campo o campo a ser testado
     * @return      <code>true</code> caso o campo tenha sido preenchido, <code>false</code> caso contrário
     */
    public static boolean validateRequiredField(Object campo) {
        return campo != null;
    }

    /**
     * Metodo responsavel por testar se um campo cujo preenchimento e tratado como erro permanece nulo.
     * Lógica inversa à do método validateRequiredField(Object).
     *
     * @param campo o campo a ser testado
     * @return      <code>true</code> caso o campo tenha sido preenchido, <code>false</code> caso contrário
     */
    public static boolean validateFieldShouldBeEmpty(Object campo) {
        return campo == null;
    }

    /**
     * Método responsável por testar se um campo de caractere foi preenchido.
     *
     * @param caractere o campo a ser testado
     * @return      <code>true</code> caso o campo tenha sido preenchido, <code>false</code> caso contrário
     */
    public static boolean validateRequiredCharacter(Character caractere) {
        return validateRequiredField(caractere) && !caractere.toString().trim().isEmpty();
    }
    
    /**
     * Método responsável por testar se um campo de texto foi preenchido.
     *
     * @param texto o campo a ser testado
     * @return      <code>true</code> caso o campo tenha sido preenchido, <code>false</code> caso contrário
     */
    public static boolean validateRequiredText(String texto) {
        return validateRequiredField(texto) && !texto.trim().isEmpty();
    }

    /**
     * Método responsável por testar se uma lista foi preenchida.
     *
     * @param lista a lista a ser testada
     * @return      <code>true</code> caso a lista tenha sido preenchida, <code>false</code> caso contrário
     */
    public static boolean validateRequiredList(List<?> lista) {
        return validateRequiredField(lista) && !lista.isEmpty();
    }

    /**
     * Método responsável por verificar se um email é válido.
     *
     * @param email o email a ser testado
     * @return      <code>true</code> caso o email informado seja válido, <code>false</code> caso contrário
     */
    public static boolean validateEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    /**
     * Responsável pelas validações associadas a períodos de tempo.
     *
     * Regras:
     * 1) O início do período deve ser anterior ao final.
     *
     * @param dataInicial   a data/hora iniciais do período
     * @param dataFinal     a data/hora finais do período
     * @return              <code>true</code> caso o período informado seja válido, <code>false</code> caso contrário
     */
    public static boolean validatePeriod(Date dataInicial, Date dataFinal) {
        boolean resultado;

        // Regra 1
        resultado = dataInicial.before(dataFinal);

        return resultado;
    }

    /**
     * Responsável por validar se a data informada (informação de hora descartada) é igual ou POSTERIOR ao momento atual (no servidor).
     *
     * @param data  a data a ser validada
     * @return      <code>true</code> caso a data informada seja igual ou POSTERIOR à atual, <code>false</code> caso contrário
     */
    public static boolean validateDateNonRetroactive(Date data) {
        Date dataPrimeiraHora = DateTimeUtil.getDateFirstHour(data); //  (informação de hora descartada)
        Date dataAtual = DateTimeUtil.getCurrentDate();
        return !dataPrimeiraHora.before(dataAtual);
    }

    /**
     * Responsável por validar se a data/hora informada é igual ou POSTERIOR ao momento atual (no servidor).
     *
     * @param dataHora  a data/hora a ser validada
     * @return      <code>true</code> caso a data informada seja igual ou POSTERIOR à atual, <code>false</code> caso contrário
     */
    public static boolean validateDateTimeNonRetroactive(Date dataHora) {
        Date dataAtual = DateTimeUtil.getCurrentDateTime();
        return !dataHora.before(dataAtual);
    }

    /**
     * Responsável por validar se a data informada (informação de hora descartada) é igual ou ANTERIOR ao momento atual (no servidor).
     *
     * @param data  a data a ser validada
     * @return      <code>true</code> caso a data informada seja igual ou ANTERIOR à atual, <code>false</code> caso contrário
     */
    public static boolean validateDateNonFuture(Date data) {
        Date dataPrimeiraHora = DateTimeUtil.getDateFirstHour(data); //  (informação de hora descartada)
        Date dataAtual = DateTimeUtil.getCurrentDate();
        return !dataPrimeiraHora.after(dataAtual);
    }

    /**
     * Responsável por validar se a data/hora informada é igual ou ANTERIOR ao momento atual (no servidor).
     *
     * @param dataHora  a data/hora a ser validada
     * @return  <code>true</code> caso a data informada seja igual ou ANTERIOR à atual, <code>false</code> caso contrário
     */
    public static boolean validateDateTimeNonFuture(Date dataHora) {
        Date dataAtual = DateTimeUtil.getCurrentDateTime();
        return !dataHora.after(dataAtual);
    }

    /**
     * Responsável por verificar se o texto informado obedece o limite de comprimento informado.
     *
     * @param quantidadeCaracteresPermitida a quantidade máxima permitida de caracteres
     * @param texto                         o texto cuja quantidade de caracteres deve ser validada
     * @return  <code>true</code> caso o texto informado obedeça o limite de caracteres informado, <code>false</code> caso contrário
     */
    public static boolean validateTextCharacterAmount(int quantidadeCaracteresPermitida, String texto) {
        return texto.length() <= quantidadeCaracteresPermitida;
    }

    /**
     * Responsável por verificar se o texto informado obedece a quantidade exata de caracteres informada.
     *
     * @param quantidadeExataCaracteresPermitida    a quantidade exata permitida de caracteres
     * @param texto                                 o texto cuja quantidade de caracteres deve ser validada
     * @return  <code>true</code> caso o texto informado obedeça a quantidade exata de caracteres informada, <code>false</code> caso contrário
     */
    public static boolean validateTextCharacterExactAmount(int quantidadeExataCaracteresPermitida, String texto) {
        return texto.length() == quantidadeExataCaracteresPermitida;
    }

    /**
     * Responsável por verificar se o valor informado está contido no domínio informado.
     *
     * @param dominio   o domínio no qual o valor informado deve estar contido
     * @param valor     o valor a ser testado para o domínio informado
     * @return  <code>true</code> caso o valor esteja contido no domínio informado, <code>false</code> caso contrário
     */
    public static boolean validateDomainPermittedValue(int[] dominio, int valor) {
        boolean resultado = false;
        for (int i = 0; i < dominio.length; i++) {
            if (valor == dominio[i]) {
                resultado = true;
                break;
            }
        }

        return resultado;
    }

    /**
     * Responsável por verificar se o valor informado está contido no domínio informado.
     *
     * @param dominio   o domínio no qual o valor informado deve estar contido
     * @param valor     o valor a ser testado para o domínio informado
     * @return  <code>true</code> caso o valor esteja contido no domínio informado, <code>false</code> caso contrário
     */
    public static boolean validateDomainPermittedValue(Character[] dominio, Character valor) {
        boolean resultado = false;
        for (int i = 0; i < dominio.length; i++) {
            if (valor == dominio[i]) {
                resultado = true;
                break;
            }
        }

        return resultado;
    }

    /**
     * Responsável por verificar se o valor informado está contido no intervalo informado.
     *
     * @param intervaloInicio   o início do intervalo permitido
     * @param intervaloFim      o final do intervalo permitido
     * @param valor             o valor a ser testado
     * @return  <code>true</code> caso o valor esteja contido no intervalo informado, <code>false</code> caso contrário
     */
    public static boolean validateValuePermittedInterval(Integer intervaloInicio, Integer intervaloFim, Integer valor) {
        return intervaloInicio <= valor && valor <= intervaloFim;
    }

    /**
     * Responsável por validar um número de CPF informado.
     *
     * @param cpf   o número de CPF a ser validado
     * @return  <code>true</code> caso o CPF informado seja válido, <code>false</code> caso contrário
     */
    public static boolean validateCpf(String cpf) {
        String cpfSemMascara = cpf.replaceAll("[^0-9]", "");

        // Testa se a quantidade de dígitos (apenas números) é adequada
        int comprimento = cpfSemMascara.length();
        if (comprimento != 11) {
            throw new IllegalArgumentException("O CPF informado possui quantidade inválida de dígitos.");
        }

        // Testa se o valor informado possui apenas números ou se encaixa no formato de CPF
        if (!cpf.matches("\\d+") && !cpf.matches("\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}")) {
            throw new IllegalArgumentException("O CPF informado está em formato desconhecido.");
        }

        boolean cpfValido = true;

        String[] listaCpfsInvalidosConhecidos = {
                "00000000000",
                "11111111111",
                "22222222222",
                "33333333333",
                "44444444444",
                "55555555555",
                "66666666666",
                "77777777777",
                "88888888888",
                "99999999999"
        };

        for (String cpfInvalido : listaCpfsInvalidosConhecidos) {
            if (cpfSemMascara.equals(cpfInvalido)) {
                cpfValido = false;
                break;
            }
        }

        String nDigResult;
        String nDigVerific;
        if (cpfValido) {
            int d1, d2;
            int digito1, digito2, resto;
            int digitoCPF;
            d1 = d2 = 0;
            digito1 = digito2 = resto = 0;
            for(int n_Count = 1; n_Count < cpfSemMascara.length() - 1; n_Count++) {
                digitoCPF = Integer.valueOf(cpfSemMascara.substring(n_Count - 1, n_Count));
                d1 = d1 + (11 - n_Count) * digitoCPF;
                d2 = d2 + (12 - n_Count) * digitoCPF;
            }
            resto = (d1 % 11);
            if(resto < 2) {
                digito1 = 0;
            } else {
                digito1 = 11 - resto;
            }
            d2 += 2 * digito1;
            resto = (d2 % 11);
            if(resto < 2) {
                digito2 = 0;
            } else {
                digito2 = 11 - resto;
            }
            nDigVerific = cpfSemMascara.substring(cpfSemMascara.length() - 2, cpfSemMascara.length());
            nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
            cpfValido = nDigVerific.equals(nDigResult);
        }

        return cpfValido;
    }

    /**
     * Responsável por validar um número de CNPJ informado.
     *
     * @param cnpj  o número de CNPJ a ser validado
     * @return  <code>true</code> caso o CNPJ informado seja válido, <code>false</code> caso contrário
     */
    public static boolean validateCnpj(String cnpj) {
        String cnpjSemMascara = cnpj.replaceAll("[^0-9]", "");

        // Testa se a quantidade de dígitos (apenas números) é adequada
        int comprimento = cnpjSemMascara.length();
        if (comprimento != 14) {
            throw new IllegalArgumentException("O CNPJ informado possui quantidade inválida de dígitos.");
        }

        // Testa se o valor informado possui apenas números ou se encaixa no formato de CNPJ
        if (!cnpj.matches("\\d+") && !cnpj.matches("\\d{2}\\.\\d{3}\\.\\d{3}/\\d{4}-\\d{2}")) {
            throw new IllegalArgumentException("O CNPJ informado está em formato desconhecido.");
        }

        // Inicializa o resultado com a comparação ao CNPJ inválido conhecido
        // (demais sequências já são rejeitadas pelo algoritmo)
        boolean cnpjValido = !cnpjSemMascara.equals("00000000000000");

        if (cnpjValido) {
            int soma = 0, dig;
            String cnpj_calc = cnpjSemMascara.substring(0, 12);
            char[] chr_cnpj = cnpjSemMascara.toCharArray();
            for(int i = 0; i < 4; i++) {
                if(chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9) {
                    soma += (chr_cnpj[i] - 48) * (6 - (i + 1));
                }
            }
            for(int i = 0; i < 8; i++) {
                if(chr_cnpj[i + 4] - 48 >= 0 && chr_cnpj[i + 4] - 48 <= 9) {
                    soma += (chr_cnpj[i + 4] - 48) * (10 - (i + 1));
                }
            }
            dig = 11 - (soma % 11);
            cnpj_calc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);
            soma = 0;
            for(int i = 0; i < 5; i++) {
                if(chr_cnpj[i] - 48 >= 0 && chr_cnpj[i] - 48 <= 9) {
                    soma += (chr_cnpj[i] - 48) * (7 - (i + 1));
                }
            }
            for(int i = 0; i < 8; i++) {
                if(chr_cnpj[i + 5] - 48 >= 0 && chr_cnpj[i + 5] - 48 <= 9) {
                    soma += (chr_cnpj[i + 5] - 48) * (10 - (i + 1));
                }
            }
            dig = 11 - (soma % 11);
            cnpj_calc += (dig == 10 || dig == 11) ? "0" : Integer.toString(dig);
            cnpjValido = cnpjSemMascara.equals(cnpj_calc);
        }

        return cnpjValido;
    }

}

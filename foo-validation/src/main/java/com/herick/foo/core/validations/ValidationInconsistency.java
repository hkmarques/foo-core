package com.herick.foo.core.validations;

import com.herick.foo.core.messages.FooMessages;

/**
 * Classe representativa dos dados associados a uma inconsistência nas validações.
 */
public abstract class ValidationInconsistency {

    /**
     * O nome/caminho da propriedade responsável pela rejeição do item.
     */
    private String propriedade;

    /**
     * A chave da mensagem associada à inconsistência na validação.
     */
    private String mensagemChave;

    /**
     * A lista de parâmetros a serem substituídos na mensagem final (quando aplicáveis).
     */
    private String[] mensagemParametros;

    /**
     * Instancia <code>ValidationInconsistency</code> inicializando os campos básicos.
     *
     * @param propriedade           o nome da propriedade associada à inconsistência
     * @param mensagemChave         a chave da mensagem associada à inconsistência
     * @param mensagemParametros    os parâmetros a serem substituídos na mensagem final associada à inconsistência
     */
    public ValidationInconsistency(String propriedade, String mensagemChave, String... mensagemParametros) {
        this.propriedade = propriedade;
        this.mensagemChave = mensagemChave;
        this.mensagemParametros = mensagemParametros;
    }

    // region # Getters and Setters

    /**
     * Obtém e retorna a mensagem associada à inconsistência de validação.
     *
     * @return  a mensagem associada a esta inconsistência de validação
     */
    public String getMensagem() {
        FooMessages messages = this.getProvedorMensagens();
        return messages.getTranslatedMessage(this.getMensagemChave(), (Object[])this.getMensagemParametros());
    }

    /**
     * Método responsável por fornecer a instância de provedor de mensagens a ser usada em traduções.
     *
     * @return a instância de provedor de mensagens a ser usada em traduções
     */
    public abstract FooMessages getProvedorMensagens();

    public String getPropriedade() {
        return propriedade;
    }

    public void setPropriedade(String propriedade) {
        this.propriedade = propriedade;
    }

    public String getMensagemChave() {
        return mensagemChave;
    }

    public void setMensagemChave(String mensagemChave) {
        this.mensagemChave = mensagemChave;
    }

    public String[] getMensagemParametros() {
        return mensagemParametros;
    }

    public void setMensagemParametros(String... mensagemParametros) {
        this.mensagemParametros = mensagemParametros;
    }

    // endregion

}

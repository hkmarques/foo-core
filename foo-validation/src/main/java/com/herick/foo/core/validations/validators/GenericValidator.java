package com.herick.foo.core.validations.validators;

import com.herick.foo.core.messages.FooMessages;
import com.herick.foo.core.validations.ValidationInconsistency;
import com.herick.foo.core.validations.excecoes.BusinessValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.LinkedList;
import java.util.List;

/**
 * Classe com implementações genéricas para validadores.
 * 
 * @param <TEntity> a entidade associada ao validador
 */
@SuppressWarnings("rawtypes")
public abstract class GenericValidator<TEntity> {

	@Autowired
    protected FooMessages fooMessages;
	
    /**
     * Armazena os dados (informados) da entidade a ser validada.
     */
	protected TEntity entityValidationData;
    
    /**
     * Armazena os dados originais da entidade a ser validada (para casos de alteração/exclusão).
     */
	protected TEntity entityOriginalData;

    /**
     * Armazena todas os inconsistências associadas às regras usadas na validação.
     */
	protected List<ValidationInconsistency> inconsistencies;

    /**
     * Armazena todos os alertas (inconsistências não impeditivas) associados às regras usadas na validação.
     */
	protected List<ValidationInconsistency> warnings;

    /**
     * Instancia um validador inicializando a lista de inconsistências.
     */
    public GenericValidator() {
        this.inconsistencies = new LinkedList<>();
        this.warnings = new LinkedList<>();
    }

    /**
     * Define a instância da entidade a ser validada.
     *
     * @param entidade a entidade a ser validada
     * @return a própria instância do validador (garante a fluência da API)
     */
    public abstract GenericValidator para(TEntity entidade);

    /**
     * Indica se o item validado foi aprovado pelas regras usadas.
     *
     * @throws BusinessValidationException caso o item não passe pelas regras de validação
     */
    public void validar() throws BusinessValidationException {
    	if (!this.inconsistencies.isEmpty()) {
            int qtdeInconsistencias = this.inconsistencies.size();
            if (qtdeInconsistencias > 1) {
                throw new BusinessValidationException(this.fooMessages.getTranslatedMessage("validacao_inconsistencia_mensagemGeral_plural", qtdeInconsistencias), this.getInconsistencies());
            } else {
                throw new BusinessValidationException(this.fooMessages.getTranslatedMessage("validacao_inconsistencia_mensagemGeral_singular"), this.getInconsistencies());
            }
        }
    }

    // region # Getters and Setters

    /**
     * Limpa a lista de inconsistências resultantes de validações já executadas.
     */
    public void limparInconsistencias() {
        this.inconsistencies.clear();
    }

    /**
     * Limpa a lista de alertas resultantes de validações já executadas.
     */
    public void limparAlertas() {
        this.warnings.clear();
    }

    public TEntity getEntityValidationData() {
        return entityValidationData;
    }

    public void setEntityValidationData(TEntity entityValidationData) {
        this.entityValidationData = entityValidationData;
    }
    
    public TEntity getEntityOriginalData() {
		return entityOriginalData;
	}

	public void setEntityOriginalData(TEntity entityOriginalData) {
		this.entityOriginalData = entityOriginalData;
	}

	public List<ValidationInconsistency> getInconsistencies() {
        return inconsistencies;
    }

    public List<ValidationInconsistency> getWarnings() {
        return warnings;
    }

    // endregion

}

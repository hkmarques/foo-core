package com.herick.foo.core.business.impl;

import com.herick.foo.core.entity.Entity;
import com.herick.foo.core.messages.FooMessages;
import com.herick.foo.core.business.GenericBusiness;
import com.herick.foo.core.validations.validators.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.List;

/**
 * Implementação genérica para as classes de negócio associadas às entidades.
 *
 * @param <TEntity>       o tipo da entidade associada à classe de negócio
 * @param <ID>              o tipo do ID da entidade
 */
@SuppressWarnings("rawtypes")
public abstract class GenericBusinessImpl<TEntity extends Entity, ID extends Serializable> implements GenericBusiness<TEntity, ID> {

    @Autowired
    FooMessages fooMessages;

    /**
     * {@inheritDoc}
     */
    public TEntity insert(TEntity businessObject) {
        throw new UnsupportedOperationException(fooMessages.getTranslatedMessage("negocio_metodo_nao_implementado_erro", "incluir"));
    }

    /**
     * {@inheritDoc}
     */
    public TEntity update(TEntity businessObject) {
        throw new UnsupportedOperationException(fooMessages.getTranslatedMessage("negocio_metodo_nao_implementado_erro", "alterar"));
    }

    /**
     * {@inheritDoc}
     */
    public void delete(TEntity businessObject) {
        throw new UnsupportedOperationException(fooMessages.getTranslatedMessage("negocio_metodo_nao_implementado_erro", "excluir"));
    }

    /**
     * {@inheritDoc}
     */
    public TEntity find(ID idEntidade) {
        throw new UnsupportedOperationException(fooMessages.getTranslatedMessage("negocio_metodo_nao_implementado_erro", "consultar"));
    }

    /**
     * Consulta a lista de todas as entradas cadastradas.
     *
     * @return  a lista de todas as entradas cadastradas
     */
    public List<TEntity> list() {
        throw new UnsupportedOperationException(fooMessages.getTranslatedMessage("negocio_metodo_nao_implementado_erro", "listar"));
    }

    /**
     * Força a implementação de um método que retorne uma instância do validador associado à entidade.
     *
     * @return  uma instância do validador associado à entidade
     */
    protected abstract GenericValidator<TEntity> getValidator();

}
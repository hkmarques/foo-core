package com.herick.foo.core.business.facades;

import com.herick.foo.core.exceptions.BusinessExceptionHandlingException;
import com.herick.foo.core.business.converters.excecoes.BusinessConverterException;
import java.io.Serializable;

/**
 * Define as capacidades básicas expostas das classes de negócio.
 * 
 * @param <TEntity>       o tipo da entidade associada à classe de negócio
 * @param <TEntityDto>    o tipo do VO associado à classe de negócio
 * @param <ID>              o tipo do ID da entidade
 */
public interface BusinessGenericFacade<TEntity, TEntityDto, ID extends Serializable> {

    /**
     * Implementação genérica do comportamento da fachada de negócio para inclusões.
     *
     * @param dto                    os dados do registro a ser incluído
     * @return                      os dados do registro incluído
     * @throws BusinessExceptionHandlingException   caso haja problemas durante a execução da operação de negócio
     * @throws BusinessConverterException            caso haja problemas durante a conversão dos dados para o negócio
     */
    TEntityDto insert(TEntityDto dto) throws BusinessExceptionHandlingException, BusinessConverterException;

    /**
     * Implementação genérica do comportamento da fachada de negócio para alterações.
     *
     * @param dto                    os dados do registro a ser alterado
     * @return                      os dados do registro alterado
     * @throws BusinessExceptionHandlingException   caso haja problemas durante a execução da operação de negócio
     * @throws BusinessConverterException            caso haja problemas durante a conversão dos dados para o negócio
     */
    TEntityDto edit(TEntityDto dto) throws BusinessExceptionHandlingException, BusinessConverterException;

    /**
     * Implementação genérica do comportamento da fachada de negócio para exclusões.
     *
     * @param dto                    os dados do registro a ser excluído
     * @throws BusinessExceptionHandlingException   caso haja problemas durante a execução da operação de negócio
     * @throws BusinessConverterException            caso haja problemas durante a conversão dos dados para o negócio
     */
    void delete(TEntityDto dto) throws BusinessExceptionHandlingException, BusinessConverterException;

    /**
     * Consulta uma entrada a partir dos dados passados como parâmetros.
     *
     * @param idEntidade o ID da entrada
     * @return os dados da entrada consultada, ou <code>null</code> caso não exista
     * @throws BusinessExceptionHandlingException   caso haja problemas durante a execução da operação de negócio
     * @throws BusinessConverterException            caso haja problemas durante a conversão dos dados para o negócio
     */
    TEntityDto find(ID idEntidade) throws BusinessExceptionHandlingException, BusinessConverterException;
    
}

package com.herick.foo.core.business.converters.impl;

import com.herick.foo.core.business.converters.GenericConverter;
import com.herick.foo.core.business.converters.excecoes.BusinessConverterException;
import org.springframework.beans.BeanUtils;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe com implementações genéricas para conversões básicas.
 *
 * Obs.: Nas conversões básicas aqui implementadas e também em todos os conversores, por via de regra,
 * consultas ao banco de dados não devem ser executadas, deixando-as a cargo das operações específicas
 * em que estas façam-se necessárias, de modo a não tornar onerosas conversões que não necessariamente
 * devem apresentar todos os dados associados a um registro.
 *
 * @param <TEntityDto> a classe usada em serviço
 * @param <TEntity> a classe usada em negócio
 */
public class GenericConverterImpl<TEntity, TEntityDto> implements GenericConverter<TEntity, TEntityDto> {
	private final Class<TEntity> classeNegocio;
	private final Class<TEntityDto> classeVo;
	
	@SuppressWarnings("unchecked")
	public GenericConverterImpl() {
		this.classeNegocio = (Class<TEntity>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		this.classeVo = (Class<TEntityDto>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}
	
	/**
	 * {@inheritDoc}
	 */
	public TEntity convertIntoBusiness(TEntityDto dto) throws BusinessConverterException {
		return this.convertIntoBusiness(dto, (String[]) null);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public TEntity convertIntoBusiness(TEntityDto dto, String... ignoredAttributes) throws BusinessConverterException {
		TEntity objNegocio = null;
		if(dto != null) {
            try {
                objNegocio = this.classeNegocio.newInstance();
            } catch (Exception exception) {
                String mensagemErroConversao = String.format("%s -> %s", classeVo.getSimpleName(), classeNegocio.getSimpleName());
                throw new BusinessConverterException(mensagemErroConversao, exception);
            }

            BeanUtils.copyProperties(dto, objNegocio, ignoredAttributes);
		}
		return objNegocio;
	}

	/**
	 * {@inheritDoc}
	 */
	public TEntityDto convertIntoDto(TEntity businessObj) throws BusinessConverterException {
		return this.convertIntoDto(businessObj, (String[]) null);
	}

	/**
	 * {@inheritDoc}
	 */
	public TEntityDto convertIntoDto(TEntity objNegocio, String... ignoredAttributes) throws BusinessConverterException {
		TEntityDto dto = null;
		if(objNegocio != null) {
            try {
                dto = this.classeVo.newInstance();
            } catch (Exception exception) {
                String mensagemErroConversao = String.format("%s -> %s", classeNegocio.getSimpleName(), classeVo.getSimpleName());
                throw new BusinessConverterException(mensagemErroConversao, exception);
            }

            BeanUtils.copyProperties(objNegocio, dto,   ignoredAttributes);
		}
		return dto;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<TEntityDto> convertListIntoDtoList(List<TEntity> businessObjList) throws BusinessConverterException {
		return this.convertListIntoDtoList(businessObjList, (String[]) null);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<TEntityDto> convertListIntoDtoList(List<TEntity> businessObjList, String... ignoredAttributes) throws BusinessConverterException {
		List<TEntityDto> resultado = null;
		if(businessObjList != null) {
			resultado = new ArrayList<TEntityDto>();
			for(TEntity objNegocio : businessObjList) {
				resultado.add(convertIntoDto(objNegocio, ignoredAttributes));
			}
		}
		return resultado;
	}
}
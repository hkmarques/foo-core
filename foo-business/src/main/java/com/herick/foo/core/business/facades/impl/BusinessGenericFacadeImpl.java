package com.herick.foo.core.business.facades.impl;

import com.herick.foo.core.exceptions.BusinessExceptionHandlingException;
import com.herick.foo.core.exceptions.annotations.ExceptionHandling;
import com.herick.foo.core.business.GenericBusiness;
import com.herick.foo.core.business.converters.GenericConverter;
import com.herick.foo.core.business.converters.excecoes.BusinessConverterException;
import com.herick.foo.core.business.facades.BusinessGenericFacade;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.PostConstruct;
import java.io.Serializable;

/**
 * Implementação genérica para as classes de negócio associadas às entidades.
 */
public abstract class BusinessGenericFacadeImpl<TEntity, TEntityDto, ID extends Serializable> implements BusinessGenericFacade<TEntity, TEntityDto, ID> {
    
    // A instância da classe de negócio usada pela fachada genérica.
    private GenericBusiness<TEntity, ID> business;

    // A instância de conversor usada pela fachada genérica.
    private GenericConverter<TEntity, TEntityDto> converter;

    /**
     * Método executado logo após a construção da instância pelo contexto do Spring.<br>
     * Aqui devem ser inicializados os campos usados nas operações da fachada genérica.<br>
     * Na criação de uma subclasse da fachada genérica, neste método deverão ser setados
     * o conversor e o negócio específicos da entidade associada à fachada.<br><br>
     * Ex. (Implementação da fachada da '<code>EntidadeAbc</code>'):
     * <pre><code>
     *
     * {@literal @}Autowired
     * private EntidadeAbcNegocio entidadeAbcNegocio;
     *
     * {@literal @}Autowired
     * private EntidadeAbcConversor entidadeAbcConversor;
     *
     * protected void init() {
     *      this.setConversor(entidadeAbcConversor);
     *      this.setBusiness(entidadeAbcNegocio);
     * }
     * </code></pre>
     */
    @PostConstruct
    protected abstract void init();

    /**
     * {@inheritDoc}
     */
    @ExceptionHandling
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public TEntityDto insert(TEntityDto dto) throws BusinessExceptionHandlingException, BusinessConverterException {
        TEntity businessObject = this.converter.convertIntoBusiness(dto);
        business.insert(businessObject);
        return converter.convertIntoDto(businessObject);
    }

    /**
     * {@inheritDoc}
     */
    @ExceptionHandling
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public TEntityDto edit(TEntityDto dto) throws BusinessExceptionHandlingException, BusinessConverterException {
        TEntity businessObject = this.converter.convertIntoBusiness(dto);
        business.update(businessObject);
        return converter.convertIntoDto(businessObject);
    }

    /**
     * {@inheritDoc}
     */
    @ExceptionHandling
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(TEntityDto dto) throws BusinessExceptionHandlingException, BusinessConverterException {
        TEntity businessObject = this.converter.convertIntoBusiness(dto);
        business.delete(businessObject);
    }

    /**
     * {@inheritDoc}
     */
    @ExceptionHandling
    public TEntityDto find(ID idEntidade) throws BusinessExceptionHandlingException, BusinessConverterException {
        return converter.convertIntoDto(business.find(idEntidade));
    }

    // region # Getters e setters

    public void setBusiness(GenericBusiness<TEntity, ID> business) {
        this.business = business;
    }

    public void setConverter(GenericConverter<TEntity, TEntityDto> converter) {
        this.converter = converter;
    }

    // endregion
}
package com.herick.foo.core.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.herick.foo.core.business.exceptions.JsonConverterException;
import com.herick.foo.core.utils.FooExceptionUtil;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Superclasse para classes de tráfego de dados entre as camadas de apresentação e de negócio.
 */
public abstract class EntityDto<ID extends Serializable> implements Serializable {

	private static final long serialVersionUID = -4947966035825737624L;

	private ObjectMapper jacksonObjectMapper = new ObjectMapper();
	
	public abstract ID getId();

    public abstract void setId(ID id);

    /**
     * Implementação genérica para método equals considerando valores de propriedades básicas.
     *
     * @param   objeto o objeto a ser comparado com o chamador
     * @return  <code>true</code> caso as propriedades básicas sejam equivalentes, <code>false</code> caso contrário
     */
    @Override
    public boolean equals(Object objeto) {
        return EqualsBuilder.reflectionEquals(this, objeto);
    }

    /**
     * Implementação genérica para método toString considerando valores de propriedades básicas.
     *
     * @return  uma string representativa do objeto chamador
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
    
    /**
     * Método para a conversão dos dados da EntityDto para o formato Json.
     *
     * @return uma string com os dados da EntityDto no formato Json.
     * @throws JsonConverterException caso haja problemas durante a conversão dos dados para o formato Json
     */
    @JsonIgnore
    public String getJson() throws JsonConverterException {
    	try {
			return this.jacksonObjectMapper.writer().writeValueAsString(this);
		} catch (IOException e) {
			throw new JsonConverterException("Erro ao converter dados do VO para o formato Json - " + FooExceptionUtil.extractExceptionMessage(e), e);
		}
    }

    /**
     * Método para a conversão dos dados da EntityDto para o formato Json.
     *
     * <pre><code>
     * EntidadeConversaoJson entidadeConversaoJson = new EntidadeConversaoJson();
     *
     * // Montando dicionário associando as classes a seus respectivos MixIns com atributos a serem ignorados durante a conversão para o formato Json
     * Map&lt;Class&lt;?&gt;,Class&lt;?&gt;&gt; entidadesVoMixIn = new HashMap&lt;Class&lt;?&gt;, Class&lt;?&gt;&gt;();
	 * entidadesVoMixIn.put(EntidadeConversaoJson.class, EntidadeConversaoJsonMixIn.class);
     *
     * // Invocando conversão passando dicionário com atributos a serem ignorados como parâmetro
     * String dadosEntidadeConversaoJson = entidadeConversaoJson.getJsonIgnoringParameters(entidadesVoMixIn);
     * </code></pre>
     *
     * @param entidadesVoMixIn dicionário associando as classes a seus respectivos MixIns com atributos a serem ignorados durante a conversão para o formato Json.
     * @return uma string com os dados da EntityDto no formato Json.
     * @throws JsonConverterException caso haja problemas durante a conversão dos dados para o formato Json
     */
    @JsonIgnore
    public String getJsonIgnoringParameters(Map<Class<?>, Class<?>> entidadesVoMixIn) throws JsonConverterException {
    	if(entidadesVoMixIn != null) {
    		Iterator<Entry<Class<?>, Class<?>>> entidadesVoMixInIterator = entidadesVoMixIn.entrySet().iterator();
    		while (entidadesVoMixInIterator.hasNext()) {
    			Map.Entry<Class<?>, Class<?>> pair = (Entry<Class<?>, Class<?>>) entidadesVoMixInIterator.next();
    			this.jacksonObjectMapper.addMixIn(pair.getKey(), pair.getValue());
    		}
    	}
    	return this.getJson();
    }

    /**
     * Método para a conversão dos dados da EntityDto para o formato Json.
     *
     * <pre><code>
     * EntidadeConversaoJson entidadeConversaoJson = new EntidadeConversaoJson();
     *
     * // Invocando conversão passando como parâmetro as classes a seus respectivos MixIns com atributos a serem ignorados durante a conversão para o formato Json
     * String dadosEntidadeConversaoJson = entidadeConversaoJson.getJsonIgnoringParameters(EntidadeConversaoJson.class, EntidadeConversaoJsonMixIn.class);
     * </code></pre>
     *
     * @param entidadesVoMixIn dicionário associando as classes a seus respectivos MixIns com atributos a serem ignorados durante a conversão para o formato Json.
     * @return uma string com os dados da EntityDto no formato Json.
     * @throws JsonConverterException caso haja problemas durante a conversão dos dados para o formato Json
     */
    @JsonIgnore
    public String getJsonIgnoringParameters(Class<?>... entidadesVoMixIn) throws JsonConverterException {
    	Map<Class<?>,Class<?>> entidadesVoMixInMap;
    	// A quantidade de argumentos passados deve ser, obrigatoriamente, par (sempre em duplas: EntityDto.class, EntityDtoMixIn.class, ...)
    	if(entidadesVoMixIn.length % 2 == 0) {
    		entidadesVoMixInMap = new HashMap<Class<?>, Class<?>>();
    		for (int i = 0; i < entidadesVoMixIn.length; i = i+2) {
    			// Os argumentos nas posições pares devem ser, obrigatoriamente, subclasses de EntityDto
    			if(EntityDto.class.isAssignableFrom(entidadesVoMixIn[i])) {
    				entidadesVoMixInMap.put(entidadesVoMixIn[i], entidadesVoMixIn[i+1]);
    			} else {
    				throw new JsonConverterException("Os argumentos devem seguir o padrão: EntidadeAVo.class, EntidadeAVoMixIn.class, EntidadeBVo.class, EntidadeBVoMixIn.class, ...");
    			}
			}
    		
    	} else {
    		throw new JsonConverterException("A quantidade de argumentos passados deve ser, obrigatoriamente, par (sempre em duplas: EntidadeAVo.class, EntidadeAVoMixIn.class, EntidadeBVo.class, EntidadeBVoMixIn.class, ...)");
    	}
    	return this.getJsonIgnoringParameters(entidadesVoMixInMap);
    }
}
package com.herick.foo.core.business.converters.excecoes;

import com.herick.foo.core.constants.FooConstants;
import java.util.ResourceBundle;

/**
 * Classe para lancar excecoes relacionadas à conversão de objetos de negócio para VOs e vice-versa.
 */
public class BusinessConverterException extends Exception {

	private static final long serialVersionUID = -7258821633324990462L;
	
	// Arquivo contendo as mensagens de erro utilizadas pela classe de exceção (i18n)
	private static final String mensagemGenericaErroConversao = ResourceBundle.getBundle(FooConstants.MESSAGE_RESOURCES_FILE).getObject("negocio_conversorgenerico_erro").toString();

	public BusinessConverterException() {
        super();
    }

    public BusinessConverterException(Throwable throwable) {
        super(throwable);
    }

    public BusinessConverterException(final String mensagem) {
        super(mensagemGenericaErroConversao + " - " + mensagem);
    }

    public BusinessConverterException(final String mensagem, Throwable throwable) {
        super(mensagemGenericaErroConversao + " - " + mensagem, throwable);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
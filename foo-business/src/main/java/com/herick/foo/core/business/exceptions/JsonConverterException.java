package com.herick.foo.core.business.exceptions;


/**
 * Classe para lancar excecoes relacionadas à conversão dos dados de VOs para o formato Json.
 */
public class JsonConverterException extends Exception {

	private static final long serialVersionUID = -7258821633324990462L;
	
	public JsonConverterException() {
        super();
    }

    public JsonConverterException(Throwable throwable) {
        super(throwable);
    }

    public JsonConverterException(final String mensagem) {
        super(mensagem);
    }

    public JsonConverterException(final String mensagem, Throwable throwable) {
        super(mensagem, throwable);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
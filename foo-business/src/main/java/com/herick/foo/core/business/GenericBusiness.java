package com.herick.foo.core.business;

import java.io.Serializable;

/**
 * Interface genérica para as classes de negócio associadas às entidades.
 *
 * @param <TEntity>       o tipo da entidade associada à classe de negócio
 * @param <ID>              o tipo do ID da entidade
 */
public interface GenericBusiness<TEntity, ID extends Serializable> {

    /**
     * Cadastra uma nova entrada a partir dos dados informados.
     *
     * @param businessObject os dados da entrada a ser cadastrada
     * @return              os dados atualizados da entrada cadastrada
     */
    TEntity insert(TEntity businessObject);

    /**
     * Atualiza uma entrada existente usando os dados informados.
     *
     * @param businessObject         os novos dados da entrada a ser atualizada
     * @return                      os dados da entrada atualizada
     */
    TEntity update(TEntity businessObject);

    /**
     * Exclui uma entrada existente a partir dos dados informados.
     *
     * @param businessObject os dados da entrada a ser excluída
     */
    void delete(TEntity businessObject);

    /**
     * Consulta uma entrada a partir dos dados passados como parâmetros.
     *
     * @param idEntidade    o ID da entrada
     * @return              os dados da entrada consultada, ou <code>null</code> caso não exista
     */
    TEntity find(ID idEntidade);
}

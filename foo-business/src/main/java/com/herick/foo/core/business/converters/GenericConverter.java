package com.herick.foo.core.business.converters;

import com.herick.foo.core.business.converters.excecoes.BusinessConverterException;
import java.util.List;

/**
 * Interface para conversores de objetos de negócio e de serviço.
 *
 * @param <TEntity> a classe usada em negócio
 * @param <TEntityDto> a classe usada pelos serviços (VO)
 */
public interface GenericConverter<TEntity, TEntityDto> {
	/**
	 * Converte uma instância de serviço em uma de negócio.
	 *
	 * @param dto o VO a ser convertido
	 * @return uma instância correspondente de negócio
	 * @throws BusinessConverterException caso haja problemas durante a conversão dos dados para o negócio
	 */
	TEntity convertIntoBusiness(TEntityDto dto) throws BusinessConverterException;
	
	/**
	 * Converte uma instância de serviço em uma de negócio.
	 *
	 * @param dto o VO a ser convertido
	 * @param ignoredAttributes parâmetros a serem ignorados durante a conversão automática
	 * @return uma instância correspondente de negócio
	 * @throws BusinessConverterException caso haja problemas durante a conversão dos dados para o negócio
	 */
	TEntity convertIntoBusiness(TEntityDto dto, String... ignoredAttributes) throws BusinessConverterException;
	
	/**
	 * Converte uma instância de negócio em uma de serviço.
	 *
	 * @param businessObj o objeto de negócio a ser convertido
	 * @return uma instância correspondente de VO
	 * @throws BusinessConverterException caso haja problemas durante a conversão dos dados para o negócio
	 */
	TEntityDto convertIntoDto(TEntity businessObj) throws BusinessConverterException;
	
	/**
	 * Converte uma instância de negócio em uma de serviço.
	 *
	 * @param objNegocio o objeto de negócio a ser convertido
	 * @param ignoredAttributes parâmetros a serem ignorados durante a conversão automática
	 * @return uma instância correspondente de VO
	 * @throws BusinessConverterException caso haja problemas durante a conversão dos dados para o negócio
	 */
	TEntityDto convertIntoDto(TEntity objNegocio, String... ignoredAttributes) throws BusinessConverterException;
	
	/**
	 * Converte uma lista de instâncias de negócio em uma correspondente de VOs.
	 *
	 * @param businessObjList a lista de objetos de negócio a serem convertidos
	 * @return uma lista de VOs com dados correspondentes à lista original
	 * @throws BusinessConverterException caso haja problemas durante a conversão dos dados para o negócio
	 */
	List<TEntityDto> convertListIntoDtoList(List<TEntity> businessObjList) throws BusinessConverterException;
	
	/**
	 * Converte uma lista de instâncias de negócio em uma correspondente de VOs.
	 *
	 * @param businessObjList a lista de objetos de negócio a serem convertidos
	 * @param ignoredAttributes parâmetros a serem ignorados durante a conversão automática
	 * @return uma lista de VOs com dados correspondentes à lista original
	 * @throws BusinessConverterException caso haja problemas durante a conversão dos dados para o negócio
	 */
	List<TEntityDto> convertListIntoDtoList(List<TEntity> businessObjList, String... ignoredAttributes) throws BusinessConverterException;
}
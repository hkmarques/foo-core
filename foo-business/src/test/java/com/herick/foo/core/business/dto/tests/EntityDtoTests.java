package com.herick.foo.core.business.dto.tests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.herick.foo.core.business.exceptions.JsonConverterException;
import com.herick.foo.core.business.dto.EntityDto;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertEquals;

/**
 * Testes associados à EntityDto
 */
public class EntityDtoTests {

	@Rule
    public ExpectedException expectedException = ExpectedException.none();
	
	// region # Casos válidos
	
	@Test
	public void testToJson() throws JsonConverterException {
		EntidadeConversaoAJson entidadeConversaoJson = new EntidadeConversaoAJson();
		entidadeConversaoJson.setId(1L);
		entidadeConversaoJson.setAtributoA("Valor do atributo A");
		entidadeConversaoJson.setAtributoB("Valor do atributo B");
		entidadeConversaoJson.setAtributoC("Valor do atributo C");
		
		assertEquals("{\"id\":1,\"atributoA\":\"Valor do atributo A\",\"atributoB\":\"Valor do atributo B\",\"atributoC\":\"Valor do atributo C\"}", entidadeConversaoJson.getJson());
	}
	
	@Test
	public void testToJsonIgnoradosMap() throws JsonConverterException {
		EntidadeConversaoBJson entidadeConversaoJson = new EntidadeConversaoBJson();
		entidadeConversaoJson.setId(1L);
		entidadeConversaoJson.setAtributoA("Valor do atributo A");
		entidadeConversaoJson.setAtributoB("Valor do atributo B");
		entidadeConversaoJson.setAtributoIgnoradoC("Valor do atributo C");
		
		Map<Class<?>,Class<?>> entidadesVoMixIn = new HashMap<Class<?>, Class<?>>();
		entidadesVoMixIn.put(EntidadeConversaoBJson.class, EntidadeConversaoBJsonMixIn.class);
		
		assertEquals("{\"id\":1,\"atributoA\":\"Valor do atributo A\",\"atributoB\":\"Valor do atributo B\"}", entidadeConversaoJson.getJsonIgnoringParameters(entidadesVoMixIn));
		
	}
	
	@Test
	public void testToJsonIgnoradosArgs() throws JsonConverterException {
		EntidadeConversaoBJson entidadeConversaoJson = new EntidadeConversaoBJson();
		entidadeConversaoJson.setId(1L);
		entidadeConversaoJson.setAtributoA("Valor do atributo A");
		entidadeConversaoJson.setAtributoB("Valor do atributo B");
		entidadeConversaoJson.setAtributoIgnoradoC("Valor do atributo C");
		
		assertEquals("{\"id\":1,\"atributoA\":\"Valor do atributo A\",\"atributoB\":\"Valor do atributo B\"}", entidadeConversaoJson.getJsonIgnoringParameters(EntidadeConversaoBJson.class, EntidadeConversaoBJsonMixIn.class));
	}
	
	//endregion
	
	// region # Casos inválidos

	@Test
	public void testToJsonIgnoradosArgsInvalidosQuantidadeImpar() throws JsonConverterException {
		
		EntidadeConversaoBJson entidadeConversaoJson = new EntidadeConversaoBJson();
		entidadeConversaoJson.setId(1L);
		entidadeConversaoJson.setAtributoA("Valor do atributo A");
		entidadeConversaoJson.setAtributoB("Valor do atributo B");
		entidadeConversaoJson.setAtributoIgnoradoC("Valor do atributo C");
		
		expectedException.expect(JsonConverterException.class);
		expectedException.expectMessage("A quantidade de argumentos passados deve ser, obrigatoriamente, par (sempre em duplas: EntidadeAVo.class, EntidadeAVoMixIn.class, EntidadeBVo.class, EntidadeBVoMixIn.class, ...)");
		
		entidadeConversaoJson.getJsonIgnoringParameters(EntidadeConversaoBJson.class, EntidadeConversaoBJsonMixIn.class, EntidadeConversaoAJson.class);
	}
	
	@Test
	public void testToJsonIgnoradosArgsInvalidosTipo() throws JsonConverterException {
		
		EntidadeConversaoBJson entidadeConversaoJson = new EntidadeConversaoBJson();
		entidadeConversaoJson.setId(1L);
		entidadeConversaoJson.setAtributoA("Valor do atributo A");
		entidadeConversaoJson.setAtributoB("Valor do atributo B");
		entidadeConversaoJson.setAtributoIgnoradoC("Valor do atributo C");
		
		expectedException.expect(JsonConverterException.class);
		expectedException.expectMessage("Os argumentos devem seguir o padrão: EntidadeAVo.class, EntidadeAVoMixIn.class, EntidadeBVo.class, EntidadeBVoMixIn.class, ...");
		
		entidadeConversaoJson.getJsonIgnoringParameters(EntidadeConversaoBJsonMixIn.class, EntidadeConversaoBJson.class);
	}

    //endregion
    
	/**
	 * Inner classes para a realização dos testes de conversão independentemente das entidades do sistema
	 */
	public class EntidadeConversaoAJson extends EntityDto<Long> {
		
		private static final long serialVersionUID = 1948273437998668120L;
		
		private Long id;
		private String atributoA;
		private String atributoB;
		private String atributoC;
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getAtributoA() {
			return atributoA;
		}
		
		public void setAtributoA(String atributoA) {
			this.atributoA = atributoA;
		}
		
		public String getAtributoB() {
			return atributoB;
		}
		
		public void setAtributoB(String atributoB) {
			this.atributoB = atributoB;
		}
		
		public String getAtributoC() {
			return atributoC;
		}
		
		public void setAtributoC(String atributoC) {
			this.atributoC = atributoC;
		}
	}
	
	public class EntidadeConversaoBJson extends EntityDto<Long> {
		
		private static final long serialVersionUID = 1948273437998668120L;
		
		private Long id;
		private String atributoA;
		private String atributoB;
		private String atributoIgnoradoC;
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getAtributoA() {
			return atributoA;
		}
		
		public void setAtributoA(String atributoA) {
			this.atributoA = atributoA;
		}
		
		public String getAtributoB() {
			return atributoB;
		}
		
		public void setAtributoB(String atributoB) {
			this.atributoB = atributoB;
		}
		
		public String getAtributoIgnoradoC() {
			return atributoIgnoradoC;
		}
		
		public void setAtributoIgnoradoC(String atributoIgnoradoC) {
			this.atributoIgnoradoC = atributoIgnoradoC;
		}
	}
	
	public class EntidadeConversaoBJsonMixIn {

		@JsonIgnore
		private String atributoIgnoradoC;
	}
	
	public class OutraEntidadeConversaoJson {
		
		private Long id;
		private String atributoA;
		private String atributoB;
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getAtributoA() {
			return atributoA;
		}
		
		public void setAtributoA(String atributoA) {
			this.atributoA = atributoA;
		}
		
		public String getAtributoB() {
			return atributoB;
		}
		
		public void setAtributoB(String atributoB) {
			this.atributoB = atributoB;
		}
	}
	
	public class OutraEntidadeConversaoJsonMixIn {

		@JsonIgnore
		private String atributoB;
	}
}